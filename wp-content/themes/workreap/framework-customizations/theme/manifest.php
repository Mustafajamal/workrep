<?php

if (!defined('FW')) {
    die('Forbidden');
}
$manifest = array();
$manifest['id'] = 'workreap';
$manifest['supported_extensions'] = array(
    'page-builder' => array(),
    'backups' => array(),
	'megamenu' => array(),
    'sidebars' => array(),
    'breadcrumbs' => array(),
    'analytics' => array(),	
);
