<?php 
/**
 *
 * The template part for displaying the freelancer profile basics
 *
 * @package   Workreap
 * @author    Amentotech
 * @link      http://amentotech.com/
 * @since 1.0
 */
global $current_user, $wp_roles, $userdata, $post;
$user_identity 	 = $current_user->ID;
$linked_profile  = workreap_get_linked_profile_id($user_identity);

$first_name 	= get_user_meta($user_identity, 'first_name', true);
$last_name 		= get_user_meta($user_identity, 'last_name', true);
$phone_no 		= get_user_meta($user_identity, 'phone_no', true);
$full_address 		= get_user_meta($user_identity, 'full_address', true);
$display_name	= $current_user->display_name;


$post_id 		= $linked_profile;
$post_object 	= get_post( $post_id );
$content 	 	= $post_object->post_content;

$per_hour_rate 	= '';
$gender  		= '';
$fiqh  			= '';
$sect  			= '';
$age  			= '';
$tag_line 		= '';

$banner_image 	= array();
if (function_exists('fw_get_db_post_option')) {
	$per_hour_rate     	= fw_get_db_post_option($post_id, '_perhour_rate', true);	
	$gender     	 	= fw_get_db_post_option($post_id, 'gender', true);	
	$fiqh     	 		= fw_get_db_post_option($post_id, 'fiqh', true);	
	$sect     	 		= fw_get_db_post_option($post_id, 'sect', true);	
	$age     	 		= fw_get_db_post_option($post_id, 'age', true);	
	$address     	 	= fw_get_db_post_option($post_id, 'address', true);	
	$tag_line     	 	= fw_get_db_post_option($post_id, 'tag_line', true);	
}

if (function_exists('fw_get_db_settings_option')) {
	$freelancer_price_option = fw_get_db_settings_option('freelancer_price_option', $default_value = null);
}

if(!empty($freelancer_price_option) && $freelancer_price_option === 'enable' ){
	$max_price     	= fw_get_db_post_option($post_id, 'max_price', true);
}

$gender_list	= array();
$gender_list	= apply_filters('workreap_gender_types',$gender_list);
$languages 		 	= workreap_get_taxonomy_array('languages');
$english_level   	= worktic_english_level_list();
$db_english_level	= get_post_meta($linked_profile, '_english_level', true);
$db_freelancer_type = get_post_meta($linked_profile, '_freelancer_type', true);
$freelancer_level   = worktic_freelancer_level_list();    

?>
<div class="wt-yourdetails wt-tabsinfo">
	<div class="wt-tabscontenttitle">
		<h2><?php esc_html_e('Your Details', 'workreap'); ?></h2>
	</div>
	<div class="wt-formtheme wt-userform">
		<fieldset>
			<?php if( !empty( $gender_list ) ){?>
			<div class="form-group form-group-half">
				<span class="wt-select">
					<select name="basics[gender]">
						<option value="" disabled=""><?php esc_html_e('Select Gender', 'workreap'); ?></option>
						<?php foreach( $gender_list as $key	=> $val ){?>
							<option <?php selected( $gender, $key, true); ?> value="<?php echo esc_attr( $key );?>"><?php echo esc_attr( $val );?></option>
						<?php }?>
					</select>
				</span>
			</div>
			<?php }?>
			
			<div class="form-group form-group-half">
				<span class="wt-select">
				
				<?php
				//echo $sect_str; 
				$sect_str = str_replace('_', ' ', $sect);
?>
					<select name="basics[sect]">
						    <option value="1"><?php esc_html_e('Sect', 'workreap'); ?></option>
							<?php if($sect  != 1){ ?>
							<option value="<?php echo esc_attr( $sect );?>" selected><?php echo ucfirst( $sect_str );?></option>
							<?php } ?>
							<option value="sunni">Sunni</option>
							<option value="ahl_hadees">Ahl Hadees</option>
							<option value="deobandi">deobandi</option>
					</select>
				</span>
			</div>			
			<div class="form-group form-group-half">
				<span class="wt-select">
				<?php
				//echo $fiqh;
				$fiqh_str = str_replace('_', ' ', $fiqh);
				?>
					<select name="basics[fiqh]">
					<option value="1" ><?php esc_html_e('Fiqh', 'workreap'); ?></option>
					<?php if($fiqh != 1){ ?>
							<option value="<?php echo esc_attr( $fiqh );?>" selected><?php echo ucfirst( $fiqh_str );?></option>
							<?php } ?>
							<option value="Hanafi">Hanafi</option>
							<option value="Maliki">Maliki</option>
							<option value="Hanbali">Hanbali</option>
							<option value="Jafari">Jafari</option>
							<option value="Zaidiyyah">Zaidiyyah</option>
							<option value="Ibadiyyah">Ibadiyyah</option>
							<option value="Zahiriyah">Zahiriyah</option>
					</select>
				</span>
			</div>

			<div class="form-group form-group-half toolip-wrapo">
				<input type="text" value="<?php echo esc_attr( $first_name ); ?>" name="basics[first_name]" class="form-control" placeholder="<?php esc_attr_e('Name', 'workreap'); ?>">
				<?php do_action('workreap_get_tooltip','element','first_name');?>
			</div>
			<!--<div class="form-group form-group-half toolip-wrapo">
				<input type="text" value="<?php //echo esc_attr( $last_name ); ?>" name="basics[last_name]" class="form-control" placeholder="<?php //esc_attr_e('Last Name', 'workreap'); ?>">
				<?php //do_action('workreap_get_tooltip','element','last_name');?>
			</div>-->
			<div class="form-group form-group-half toolip-wrapo">
				<input type="text" value="<?php echo esc_attr( $phone_no ); ?>" name="basics[phone_no]" class="form-control" placeholder="<?php esc_attr_e('Last Name', 'workreap'); ?>">
			</div>
			<div class="form-group form-group-half toolip-wrapo">
				<input type="text" value="<?php if($fiqh != 1) echo esc_attr( $age ); ?>" name="basics[age]" class="form-control" placeholder="<?php esc_attr_e('Age', 'workreap'); ?>">
			</div>			
			<div class="form-group form-group-half toolip-wrapo">
				<input type="text" value="<?php echo esc_attr( $address ); ?>" name="basics[address]" class="form-control" placeholder="<?php esc_attr_e('Address', 'workreap'); ?>">
			</div>
			<div class="form-group form-group-half toolip-wrapo">
				<input type="text" name="basics[display_name]" class="form-control" value="<?php echo esc_attr( $display_name ); ?>" placeholder="<?php esc_attr_e('Display name', 'workreap'); ?>">
				<?php do_action('workreap_get_tooltip','element','display_name');?>
			</div>

			
			<div class="form-group toolip-wrapo form-group-half">
				<input type="number" name="basics[per_hour_rate]" class="form-control" value="<?php if($per_hour_rate != 0) echo intval( $per_hour_rate ); ?>" placeholder="<?php esc_attr_e('Minimum hourly rate ($)', 'workreap'); ?>">
				<?php do_action('workreap_get_tooltip','element','perhour');?>
			</div>
			<div class="form-group">
				<textarea name="basics[content]" class="form-control" placeholder="<?php esc_attr_e('Description', 'workreap'); ?>"><?php echo esc_textarea( $content ); ?></textarea>
			</div>
		</fieldset>
	</div>

</div>
<div class="wt-tabsinfo">
	<div class="wt-tabscontenttitle">
		<h2><?php esc_html_e('Spoken Language', 'workreap'); ?></h2>
	</div>
	<div class="wt-settingscontent">
		<div class="wt-formtheme wt-userform">
			<div class="form-group">
				<select data-placeholder="<?php esc_attr_e('Spoken Language', 'workreap'); ?>" name="settings[languages][]" multiple class="chosen-select">
					<?php if( !empty( $languages ) ){
						foreach( $languages as $key => $item ){
							$selected = '';
							if( has_term( $item->term_id, 'languages', $post_id )  ){
								$selected = 'selected';
							}
						?>
						<option <?php echo esc_attr($selected);?> value="<?php echo intval( $item->term_id );?>"><?php echo esc_html( $item->name );?></option>
					<?php }}?>
				</select>
			</div>
		</div>
	</div>
	<?php
	/* 
	<div class="wt-tabscontenttitle">
		<!--<h2><?php //esc_html_e('English Level', 'workreap'); ?></h2>-->
		<h2><?php esc_html_e('Subject', 'workreap'); ?></h2>
	</div>
	<div class="wt-settingscontent">
		<div class="wt-formtheme wt-userform">
			<div class="form-group">
				<select data-placeholder="<?php esc_attr_e('Subject', 'workreap'); ?>" name="settings[english_level]" class="chosen-select">
					<?php if( !empty( $english_level ) ){
						foreach( $english_level as $key => $item ){
					?>
					<option <?php selected( $db_english_level, $key ); ?> value="<?php echo esc_attr( $key );?>"><?php echo esc_html( $item );?></option>
					<?php }}?>
				</select>
			</div>
		</div>
	</div> */
	?>
	<div style="display: none;" class="wt-tabscontenttitle">
		<h2><?php esc_html_e('Freelancer Type', 'workreap'); ?></h2>
	</div>
	<div style="display: none;" class="wt-settingscontent">
		<div class="wt-formtheme wt-userform">
			<div class="form-group">
				<select data-placeholder="<?php esc_attr_e('Freelancer Type', 'workreap'); ?>" name="settings[freelancer_type]" class="chosen-select">
					<?php if( !empty( $freelancer_level ) ){
						foreach( $freelancer_level as $key => $item ){
					?>
					<option <?php selected( $db_freelancer_type, $key ); ?> value="<?php echo esc_attr( $key );?>"><?php echo esc_html( $item );?></option>
					<?php }}?>
				</select>
			</div>
		</div>
	</div>
</div>

<style>
.chosen-container .chosen-results{
	max-height:80px;
	z-index:999;
}
.wt-companysinfo .wt-btnarea {
    display: none !important;
}
</style>