<?php
/**
 *
 * The template used for displaying freelancer post basics
 *
 * @package   Workreap
 * @author    amentotech
 * @link      https://amentotech.com/user/amentotech/portfolio
 * @version 1.0
 * @since 1.0
 */

global $post,$current_user;

$post_id 					= $post->ID; 
$user_id					= workreap_get_linked_profile_id( $post_id, 'post' );
if (function_exists('fw_get_db_post_option')) {
	$tag_line			= fw_get_db_post_option($post_id, 'tag_line', true);
	$freelancer_stats 	= fw_get_db_settings_option('freelancer_stats');
	$application_access = fw_get_db_settings_option('application_access');
} else {
	$tag_line			= "";
	$freelancer_stats	= "show";
}

$application_access	= !empty( $application_access ) ? $application_access : '';

$basic_content	= 'withoutstats';
if( isset( $freelancer_stats ) && $freelancer_stats === 'show' ){
	$basic_content	= 'withstats';
}

$content					= get_the_content();
$completed_jobs				= workreap_count_posts_by_meta( 'projects' ,'', '_freelancer_id', $post_id, 'completed');
$total_completed_jobs		= !empty($completed_jobs) ? $completed_jobs : 0;

$ongoing_jobs				= workreap_count_posts_by_meta( 'projects' ,'', '_freelancer_id', $post_id, 'hired');
$total_ongoing_jobs			= !empty($ongoing_jobs) ? $ongoing_jobs : 0;

$cancelled_jobs				= workreap_count_posts_by_meta( 'proposals' ,$user_id, '', '', 'cancelled');
$total_cancelled_jobs		= !empty($cancelled_jobs) ? $cancelled_jobs : 0;

$earnings					= workreap_get_sum_payments_freelancer($user_id,'completed','amount');
$earnings					= !empty($earnings) ? $earnings : 0;


$completed_services			= workreap_count_posts_by_meta( 'services-orders' ,'', '_service_author', $user_id, 'completed');
$total_completed_services	= !empty($completed_services) ? $completed_services : 0;

$ongoing_services			= workreap_count_posts_by_meta( 'services-orders' ,'', '_service_author', $user_id, 'hired');
$total_ongoing_services		= !empty($ongoing_services) ? $ongoing_services : 0;

$cancelled_services			= workreap_count_posts_by_meta( 'services-orders' ,'', '_service_author', $user_id, 'cancelled');
$total_cancelled_services	= !empty($cancelled_services) ? $cancelled_services : 0;

$freelancer_avatar = apply_filters(
		'workreap_freelancer_avatar_fallback', workreap_get_freelancer_avatar( array( 'width' => 225, 'height' => 225 ), $post_id ), array( 'width' => 225, 'height' => 225 )
	);
$socialmediaurls	= array();
if( function_exists('fw_get_db_settings_option')  ){
	$socialmediaurl	= fw_get_db_settings_option('freelancer_social_profile_settings', $default_value = null);
}
$socialmediaurl 		= !empty($socialmediaurls) ? $socialmediaurls['gadget'] : '';

$social_settings	= array();
if (function_exists('workreap_get_social_media_icons_list')){
	$social_settings	= workreap_get_social_media_icons_list('no');
}

$applicationClass	= 'wt-access-both';
if( $application_access === 'job_base' ){
	$applicationClass	= 'wt-access-jobs';
} else if( $application_access === 'service_base' ){
	$applicationClass	= 'wt-access-services';
}
$freelancer_title 		= workreap_get_username('',$post_id); 

if (isset($_POST['send_offer_email'])) {
	//Send email to users
	// if (class_exists('Workreap_Email_helper')) {
	// 	if (class_exists('WorkreapHireFreelancer')) {
	// 		$email_helper 	= new WorkreapHireFreelancer();
	// 		$emailData 	  	= array();
	// 		$employer_id	= workreap_get_linked_profile_id($employer_id_user);
	// 		$freelancer_id	= get_post_meta($order_detail['proposal_id'],'_send_by',true);
	// 		$profile_id		= workreap_get_linked_profile_id($freelancer_id,'post');
	// 		$user_email 	= !empty( $profile_id ) ? get_userdata( $profile_id )->user_email : '';

	// 		$emailData['freelancer_link'] 		= esc_url( get_the_permalink( $freelancer_id ));
	// 		$emailData['freelancer_name'] 		= esc_html( get_the_title($freelancer_id));
	// 		$emailData['employer_link']       	= esc_url( get_the_permalink( $employer_id ) );
	// 		$emailData['employer_name'] 		= esc_html( get_the_title($employer_id));
	// 		$emailData['project_link']        	= $project_link;
	// 		$emailData['project_title']      	= $project_title;
	// 		$emailData['email_to']      		= $user_email;
	// 		$emailData['project_id']      		= $project_id;
	// 		$emailData['employer_id']      		= $employer_id;
	// 		$emailData['freelancer_id']      	= $freelancer_id;
	// 		$email_helper->send_hire_freelancer_email($emailData);
	// 	}
	// }
	$to = 'cricketoverflow@gmail.com';
	//print_r($to);
	$subject = 'The subject';
	$body = "Teacher: '".$freelancer_title."'<br> Name: '".$_POST['client_name']."'<br> Email: '".$_POST['client_email']."'<br> Subject: '".$_POST['client_subject']."'<br> Language: '".$_POST['client_language']."'";
	$headers = array('Content-Type: text/html; charset=UTF-8');
	 
	wp_mail( $to, $subject, $body, $headers );

}?>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left">
			<div class="wt-userprofileholder">
				<?php do_action('workreap_featured_freelancer_tag',$user_id);?>
				<div class="col-12 col-sm-12 col-md-12 col-lg-3 float-left">
					<div class="row">
						<div class="wt-userprofile">
							<figure>
								<img src="<?php echo esc_url( $freelancer_avatar );?>" alt="<?php esc_attr_e('freelancer','workreap');?>">
								<?php echo do_action('workreap_print_user_status',$user_id);?>
							</figure>
							<?php
						/* 	<div class="wt-title">
								<h1><i class="fa fa-check-circle"></i><?php echo esc_attr( $freelancer_title );?></h1>
								<div class="wt-sinle-pmeta">
									<?php do_action('workreap_freelancer_get_reviews',$post_id,'v1');?>
									<span class="wtmember-since"><?php esc_html_e('Member since','workreap');?>&nbsp;<?php echo get_the_date( get_option('date_format') );?></span>
									<?php
										if(!empty($social_settings) && !empty($socialmediaurl) && $socialmediaurl === 'enable') { ?>
											<ul class="wt-socialiconssimple">
												<?php
													foreach($social_settings as $key => $val ) {
														$icon		= !empty( $val['icon'] ) ? $val['icon'] : '';
														$color			= !empty( $val['color'] ) ? $val['color'] : '#484848';

														$enable_value   = !empty($socialmediaurls['enable'][$key]['gadget']) ? $socialmediaurls['enable'][$key]['gadget'] : '';
														if( !empty($enable_value) && $enable_value === 'enable' ){ 
															$social_url	= '';
															if( function_exists('fw_get_db_post_option') ){
																$social_url	= fw_get_db_post_option($post_id, $key, true);
															}
															if(!empty($social_url)) {?>
																<li>
																	<a href="<?php echo esc_url($social_url); ?>" target="_blank">
																		<i class="tg-icon <?php echo esc_attr( $icon );?>" style="color:<?php echo esc_attr( $color );?>"></i>
																	</a>
																</li>
															<?php } ?>
														<?php } ?>
													<?php } ?>
											</ul>
										<?php } ?>
								</div>
							</div>
						*/
							?>
							
							
							<?php if( isset( $freelancer_stats ) && $freelancer_stats === 'hide' ){?>
							<div class="wt-description <?php echo esc_attr( $basic_content );?>">
								<?php if( is_user_logged_in() ) {?>
									<a class="wt-btn wt-send-offers" href="javascript:;">
										<?php esc_html_e('Send Offer','workreap');?>
									</a>
								<?php } else {?>
									<a class="wt-btn wt-loginfor-offer" href="javascript:;">
										<?php esc_html_e('Send  Offer','workreap');?>
									</a>
								<?php } ?>
							</div>
							<?php }?>
						</div>
					</div>
				</div>
				<?php
			$per_hour_rate = fw_get_db_post_option($post_id, '_perhour_rate', true);	
			$gender = fw_get_db_post_option($post_id, 'gender', true);	
			$fiqh = fw_get_db_post_option($post_id, 'fiqh', true);	
			$sect = fw_get_db_post_option($post_id, 'sect', true);
			$fiqh_str = str_replace('_', ' ', $fiqh);
			$sect_str = str_replace('_', ' ', $sect);
			$age= fw_get_db_post_option($post_id, 'age', true);	
			$specialization_list= fw_get_db_post_option($post_id, 'specialization', true);	
			$db_english_level = get_post_meta($post_id, '_english_level', true);
			$term_lang_list = get_the_terms( $post_id, 'languages' );
			$specialization = count($specialization_list);
			$language_count = count($term_lang_list);


	?>
				<div class="col-12 col-sm-12 col-md-12 col-lg-9 float-left <?php echo esc_attr( $basic_content );?>">
					<div class="row">
						<div class="wt-profile-content-holder">
							<div class="wt-proposalhead wt-userdetails">
							<h1><?php echo esc_attr( $freelancer_title );?></h1>
								<?php if( !empty( $tag_line ) ){?><h2><?php echo esc_html(stripslashes($tag_line));?></h2><?php } ?>
								<?php //do_action('workreap_freelancer_breadcrumbs',$post_id,'wt-userlisting-breadcrumbvtwo');
								 ?>
									<?php if( !empty( $content ) ){?>
									<div class="wt-description">
										<?php the_content(); ?>
									</div>
								<?php }
								?>
							</div>
							<?php if( isset( $freelancer_stats ) && $freelancer_stats === 'show' ){?>
								<div id="wt-statistics" class="wt-statistics wt-profilecounter <?php echo esc_attr( $applicationClass );?>">
									<?php //if( $application_access === 'job_base' ){ ?>
										<div class="wt-statisticcontent wt-countercolor1">
											<h3 ><?php echo ucfirst($sect_str);?></h3>
											<h4><?php esc_html_e('Sect','workreap');?></h4>
										</div>
										<div class="wt-statisticcontent wt-countercolor2">
											<h3><?php echo ucfirst($gender); ?></h3>
											<h4><?php esc_html_e('Gender','workreap'); ?></h4>
										</div>
										<div class="wt-statisticcontent wt-countercolor4">
											<h3><?php echo ucfirst($fiqh_str); ?></h3>
											<h4><?php esc_html_e('Fiqh','workreap');?></h4>
										</div>
									<?php // }else if( $application_access === 'service_base'){?>
										<div class="wt-statisticcontent wt-countercolor1">
											<h3 ><?php echo ucfirst($age); ?></h3>
											<h4><?php esc_html_e('Age','workreap');?></h4>
										</div>
										<div class="wt-statisticcontent wt-countercolor2">
											<h3><?php echo ucfirst($specialization); ?></h3>
											<h4><?php esc_html_e('Subjects','workreap'); ?></h4>
										</div>
										<div class="wt-statisticcontent wt-countercolor4">
											<h3><?php echo intval($language_count);?></h3>
											<h4><?php esc_html_e('Language','workreap'); ?></h4>
										</div>
					
									<div class="wt-description">
										<p><?php esc_html_e('* Click the button to send an offer','workreap');?></p>
										<?php if( is_user_logged_in() ) {?>										
											
									<div class="wt-formtheme wt-userform popup_model wt-registration-poup-custom">
										
										<div class="popup_content">
											<span class="popup_close">X</span>
										<fieldset>
											<form method="post" action=""  enctype="multipart/form-data">
											<h3><?php echo esc_attr( $freelancer_title );?></h3>
											<div class="form-group form-group-half toolip-wrapo">
												<input type="text" value="" name="client_name" class="form-control" placeholder="<?php esc_attr_e('Name', 'workreap'); ?>">
											</div>
											<div class="form-group form-group-half toolip-wrapo">
												<input type="text" value="" name="client_phone" class="form-control" placeholder="<?php esc_attr_e('Phone', 'workreap'); ?>">
											</div>
											<div class="form-group form-group-half toolip-wrapo">
												<input type="text" value="" name="client_email" class="form-control" placeholder="<?php esc_attr_e('Email', 'workreap'); ?>">
											</div>
											<div class="form-group form-group-half toolip-wrapo">
												<input type="text" value="" name="client_subject" class="form-control" placeholder="<?php esc_attr_e('Subject', 'workreap'); ?>">
											</div>
											<div class="form-group form-group-half toolip-wrapo">
												<input type="text" value="" name="client_language" class="form-control" placeholder="<?php esc_attr_e('Language', 'workreap'); ?>">
											</div>
											<div class="form-group form-group-full toolip-wrapo">
												<input type="submit" value="Send Offer" name="send_offer_email" class=" wt-btn form-control" placeholder="<?php esc_attr_e('Send Offer', 'workreap'); ?>">
											</div>
											</form>
										</fieldset>
										</div>		
									</div>		
									<a class="wt-btn send_offer_btn" href="javascript:;">
										<?php esc_html_e('Send Offer','workreap');?>
									</a>
									<!--<a class="wt-btn wt-send-offers" href="javascript:;">
										<?php //esc_html_e('Send Offer3','workreap');?>
									</a>-->
										<?php } ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.popup_content span.popup_close {
	    position: sticky;
	    right: 0px;
	    float: right;
	    cursor: pointer;
	}
	.wt-formtheme.wt-userform.popup_model.wt-registration-poup-custom {
	    display: none;
	    position: fixed;
	    z-index: 1;
	    left: 0;
	    top: 0;
	    width: 100%;
	    height: 100%;
	    overflow: auto;
	    background-color: rgb(0,0,0);
	    background-color: rgba(0,0,0,0.4);
	}
	.popup_content {
	    background: #fefefe;
	    margin: 15% auto;
	    padding: 20px;
	    border: 1px solid #888;
	    width: 80%;
	    display: inline-block;
	}

span.strong {
    font-weight: 700;
    color: #323232;
    margin: 0px 8px 8px 0px;
    font-weight: 600;
    line-height: 1.5em;
    font-style: normal;
    font-size: 14px;
    font-family: 'Poppins',Arial,Helvetica,sans-serif;
}
ul.styles .strong {
    margin-right: 15px;
}
ul.styles li {
    list-style: none;
    display: inline-block;
    margin-right: 5px;
    background: #eee;
   color: #000;
    border: 1px solid #000;
    padding: 0px 5px;
    border-radius: 5px;
}
ul.styles a,ul.styles li {
	font-size:14px;
	cursor:auto;
	color:#000;
}
.gender_div {
    margin-top: 10px;
}
.wt-companysinfo .wt-btnarea {
    display: none !important;
}
.chosen-container .chosen-results{
	max-height:80px;
	z-index:999;
}
</style>
<script type="text/javascript">
	jQuery(document).ready(function(){  

		//Registration Popup
		jQuery(document).on('click', '.send_offer_btn', function (event) {
			jQuery(".wt-registration-poup-custom").show();
			
		});
		//Popup Close
		jQuery(document).on('click', '.wt-registration-poup-custom .popup_close', function (event) {
			jQuery(".wt-registration-poup-custom").hide();
			
		});

	});
</script>