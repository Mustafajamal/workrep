<?php
/**
 *
 * The template used for displaying freelancer Skills
 *
 * @package   Workreap
 * @author    amentotech
 * @link      https://amentotech.com/user/amentotech/portfolio
 * @version 1.0
 * @since 1.0
 */

global $post;
$post_id 		= $post->ID;

if (function_exists('fw_get_db_post_option')) {
	$awards 	= fw_get_db_post_option($post_id, 'awards', true);
} else {
	$awards		= array();
}

if( !empty( $awards ) && is_array($awards) ){?>
	<div class="wt-widget wt-widgetarticlesholder wt-articlesuser items-more-wrap-aw">
		<div class="wt-widgettitle">
			<h2><?php esc_html_e('Awards & Certifications','workreap');?></h2>
		</div>
		<div class="wt-widgetcontent data-list">
			<?php foreach( $awards as $key => $item ){?>
			<div class="wt-particlehold sp-load-item">
				<?php if( !empty( $item['image']['url'] ) ){?>
					<figure> <img src="<?php echo esc_url( $item['image']['url'] );?>" alt="<?php esc_attr_e('certificate','workreap');?>"></figure>
				<?php }?>
				<div class="wt-particlecontent">
					<h3><a href="javascript:;"><?php echo esc_html( $item	['title'] );?></a></h3>
					<?php if( !empty( $item['date'] ) ){?>
						<span><i class="lnr lnr-calendar"></i><?php echo date_i18n(get_option('date_format'), strtotime($item['date'])); ?></span>
					<?php }?>
				</div>
			</div>
			<?php }?>
			<div class="wt-btnarea"><a href="javascript:;" class="sp-loadMore"><?php esc_html_e('Load More','workreap');?></a></div> 
		</div>
	</div>
<?php }?>