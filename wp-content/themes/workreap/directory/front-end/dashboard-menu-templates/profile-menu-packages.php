<?php
/**
 *
 * The template part for displaying the dashboard menu
 *
 * @package   workreap
 * @author    Amentotech
 * @link      http://amentotech.com/
 * @since 1.0
 */

global $current_user, $wp_roles, $userdata, $post;

$reference 		 = (isset($_GET['ref']) && $_GET['ref'] <> '') ? $_GET['ref'] : '';
$mode 			 = (isset($_GET['mode']) && $_GET['mode'] <> '') ? $_GET['mode'] : '';
$user_identity 	 = $current_user->ID;
if( apply_filters('workreap_is_feature_allowed', 'packages', $user_identity) === false ){
?>
<li class="<?php echo esc_attr( $reference === 'packages' ? 'tg-active' : ''); ?>">
	<a href="<?php Workreap_Profile_Menu::workreap_profile_menu_link('package', $user_identity); ?>">
		<i class="ti-package"></i>
		<span><?php esc_html_e('Packages','workreap');?></span>
	</a>
</li>
<?php }
