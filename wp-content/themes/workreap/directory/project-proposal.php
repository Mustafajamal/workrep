<?php 
/**
 * Template Name: Project Proposal
 *
 * @package Workreap
 * @since Workreap 1.0
 * @desc Template used for front end proposal submission.
 */
get_header();
global $current_user;
$project_id 		= !empty( $_GET['project_id'] ) ? $_GET['project_id'] : '';

if( !empty( $project_id ) && get_post_type( $project_id ) == 'projects' ){
	$project_title 		= esc_html( get_the_title( $project_id ));
	$service_fee 		= '';
	$service_hint 		= '';
	$deduction_hint 	= '';
	$price_symbol		= workreap_get_current_currency();
	
	$price_symbol		= !empty($price_symbol['symbol']) ? $price_symbol['symbol'] : '$';
		
	if (function_exists('fw_get_db_post_option')) {
		$service_fee    	= fw_get_db_settings_option('service_fee');
		$service_hint    	= fw_get_db_settings_option('hint_text');
		$deduction_hint    	= fw_get_db_settings_option('hint_text_two');
		$db_project_type    = fw_get_db_post_option($project_id,'project_type');
	}
	
	if( !empty( $db_project_type['gadget'] ) && $db_project_type['gadget'] === 'fixed' ){
		$project_cost = !empty( $db_project_type['fixed']['project_cost'] ) ? $db_project_type['fixed']['project_cost'] : '';
		$amount_text	= esc_html__('Enter Your Proposal Amount','workreap');
	} else if( !empty( $db_project_type['gadget'] ) && $db_project_type['gadget'] === 'hourly' ){
		$estimated_hours 	= !empty( $db_project_type['hourly']['estimated_hours'] ) ? $db_project_type['hourly']['estimated_hours'] : 0;
		$project_cost 		= !empty( $db_project_type['hourly']['hourly_rate'] ) ? $estimated_hours * ($db_project_type['hourly']['hourly_rate'] ) : 0;
		$amount_text		= esc_html__('Enter Your Per Hour rate','workreap');
	}
	
	$project_price	= workreap_project_price($project_id);
	
	if( isset( $project_price['type'] ) && $project_price['type'] === 'hourly' ){
		$project_cost	= !empty($project_price['cost']) ? $project_price['cost'].' '.esc_html__('Per hour','workreap').' '.esc_html__('(For','workreap').' '.$estimated_hours.' '.esc_html__('hours)','workreap') : 0;
	} else{
		$project_cost	= !empty($project_price['cost']) ? $project_price['cost'] : 0;
	}
	
	$max_val		= !empty($project_price['max_val']) ? $project_price['max_val'] : 0;
	$amount_text	= !empty($project_price['amount_text']) ? $project_price['amount_text'] : '';

	$list              	= worktic_job_duration_list();
	$blog_name		 	= get_bloginfo( 'name' );			

	//Get Project Skills	
	$project_skills 	= array();	
	$freelancer_skills 	= array();
	$skills_matched 	= array();
	$skills_string 		= '';
	$projects_skills = wp_get_post_terms($project_id, 'skills', true);		
	
	if( taxonomy_exists('skills') ) {
		if( !empty( $projects_skills ) ){
			foreach ( $projects_skills as $key => $value ) {
				$project_skills[] = $value->term_id;
				$skills_string	.= '“'.$value->name.'”&nbsp;';
			}

			//Get Freelancer Skills
			$post_id = workreap_get_linked_profile_id($current_user->ID);			
			if( !empty( $post_id ) ){
				$skills = wp_get_post_terms($post_id, 'skills', true);		
				if( !empty( $skills ) ){
					foreach ( $skills as $key => $value ) {
						$freelancer_skills[] = $value->term_id;				
					}
				}		
			}

			//Comparision		
			$skills_matched = array_intersect( $project_skills, $freelancer_skills );			
		} else {
			//If project skills not set
			$skills_matched[0] = 'set';
		} 
	}
	
	if (function_exists('fw_get_db_settings_option')) {
		$job_price_option           = fw_get_db_settings_option('job_price_option', $default_value = null);
	}
	
	$job_price_option 			= !empty($job_price_option) ? $job_price_option : '';
	
	if(!empty($job_price_option) && $job_price_option === 'enable') {
		$db_max_price      = fw_get_db_post_option($post->ID,'max_price');
		$place_holder	= esc_attr__('Minimum Price','workreap');
	} else{
		$place_holder	= esc_attr__('Project Price','workreap');
	}

	$is_visible	= 'yes';
	?>
	<div class="wt-haslayout wt-proposal-single">	
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 push-lg-2">
					<?php
					if( is_user_logged_in() ) {
						$user_type		= apply_filters('workreap_get_user_type', $current_user->ID );
						?>
						<div class="wt-jobalertsholder">
							<?php 
								if ( $user_type === 'freelancer' ) {
									if(  empty( $skills_matched ) ) {
										$message	= esc_html__('You’ve no skills of','workreap');
										$skills     = esc_html( $skills_string ); 
										$message_v2	= esc_html__('but still you can apply for this project.', 'workreap');
										$final 		= $message.' '.$skills.' '.$message_v2;
										$title		= esc_html__('Info','workreap');
										Workreap_Prepare_Notification::workreap_warning($title, $final, '', '');								
									} 
									
									if( apply_filters('workreap_is_feature_allowed', 'packages', $current_user->ID) === false ){
										if( apply_filters('workreap_feature_connects', $current_user->ID) === false ){
											$link		= Workreap_Profile_Menu::workreap_profile_menu_link('package', $current_user->ID,true);
											$message	= esc_html__('You’ve consumed all your credits to apply on this job.','workreap');
											$title		= esc_html__('Alert :','workreap');
											Workreap_Prepare_Notification::workreap_warning($title, $message, $link, esc_html__("Buy Now",'workreap'));								
										}
									}
								} else {
									$is_visible	= 'no';
									$message	= esc_html__('You are not allowed to submit the proposal on the job','workreap');
									$title		= esc_html__('Info','workreap');
									Workreap_Prepare_Notification::workreap_warning($title, $message, '', '');
								}
							?>
						</div>
					<?php }?>
					<?php if( isset( $is_visible ) && $is_visible === 'yes' ){?>
						<div class="wt-proposalholder">					
							<div class="wt-proposalhead">
								<h2><?php echo esc_html( $project_title ); ?></h2>
								<?php do_action( 'workreap_job_detail_header', $project_id ); ?>
							</div>
						</div>
						<form class="wt-proposalamount-holder wt-send-project-proposal">
							<div class="wt-title">
								<h2><?php esc_html_e('Proposal Amount', 'workreap'); ?></h2>
							</div>
							<div class="wt-proposalamount accordion">
								<div class="form-group">
									<span>(<i><?php echo esc_attr($price_symbol);?></i> )</span>
									<input type="number" name="proposed_amount" class="form-control wt-proposal-amount" min="0" max="<?php echo intval($max_val);?>" placeholder="<?php echo esc_attr($amount_text); ?>">
									<a href="javascript:;" class="collapsed" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="lnr lnr-chevron-up"></i></a>
									<?php if( !empty( $db_project_type['gadget'] ) && $db_project_type['gadget'] === 'hourly' ){ ?>
										<input type="number" name="estimeted_time" class="form-control wt-estimated-hours" min="0" placeholder="<?php echo esc_attr_e('Estimated Hours','workreap'); ?>">
									<?php } ?>
									<em><?php esc_html_e('Total amount the client will see on your proposal', 'workreap'); ?></em>
								</div>
								<ul class="wt-totalamount collapse show" id="collapseOne" aria-labelledby="headingOne">
									<?php if( !empty($project_cost) ){?>
									<li>
										<h3>(<i><?php echo esc_attr($price_symbol);?></i> ) <em class="wt-project-cost"><?php echo esc_html($project_cost); ?></em></h3>
										<span><strong><?php esc_html_e('Employer’s Proposed Project Cost', 'workreap'); ?></strong></span>
									</li>
									<li>
										<h3>(<i><?php echo esc_attr($price_symbol);?></i> ) <em class="wt-project-proposed"><?php esc_html_e('0.00', 'workreap'); ?></em></h3>
										<span><strong><?php esc_html_e('Your proposed project cost', 'workreap'); ?></strong></span>
									</li>
									<?php }?>
									<?php if( !empty( $service_fee ) ){?>
										<li>
											<h3>(<i><?php echo esc_attr($price_symbol);?></i> ) <em class="wt-service-fee"><?php esc_html_e('- 00.00', 'workreap'); ?></em></h3>
											<span><strong><?php echo esc_html( $blog_name ); ?></strong> <?php esc_html_e('Service Fee', 'workreap'); ?><?php if( !empty( $service_hint ) ){ ?><i class="fa fa-exclamation-circle template-content tipso_style wt-tipso" data-tipso="<?php echo esc_attr( $service_hint ); ?>"></i><?php } ?></span>
										</li>
										<li>
											<h3>(<i><?php echo esc_attr($price_symbol);?></i> ) <em class="wt-user-amount"><?php esc_html_e('- 00.00', 'workreap'); ?></em></h3>
											<span><?php esc_html_e('Amount You’ll Receive after', 'workreap'); ?> <strong><?php echo esc_html( $blog_name ); ?></strong> <?php esc_html_e('Service Fee deduction', 'workreap'); ?><?php if( !empty( $deduction_hint ) ){ ?><i class="fa fa-exclamation-circle template-content tipso_style wt-tipso" data-tipso="<?php echo esc_attr( $deduction_hint ); ?>"></i><?php } ?></span>
										</li>
									<?php }?>
								</ul>
							</div>
							<div class="wt-formtheme wt-formproposal">
								<fieldset>
									<?php if( !empty( $db_project_type['gadget'] ) && $db_project_type['gadget'] === 'fixed' ){ ?>
										<div class="form-group">
											<span class="wt-select">
												<select name="proposed_time">
													<option value=""><?php esc_html_e('Select time period', 'workreap'); ?></option>
													<?php foreach ($list as $key => $value) { ?>
														<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
													<?php } ?>										
												</select>
											</span>
										</div>
									<?php } ?>
									<div class="form-group">
										<textarea name="proposed_content" class="form-control" placeholder="<?php esc_attr_e('Add Description*', 'workreap'); ?>"></textarea>
									</div>
								</fieldset>
								<div class="wt-formtheme wt-formprojectinfo wt-formcategory">
									<fieldset>
										<div class="form-group form-group-label" id="wt-proposal-container">
											<div class="wt-labelgroup" id="proposal-drag">
												<label class="wt-proposal-file">
													<span class="wt-btn" id="proposal-btn"><?php esc_html_e('Select File', 'workreap'); ?></span>						
												</label>
												<span><?php esc_html_e('Drop files here to upload', 'workreap'); ?></span>
											</div>
										</div>
										<div class="form-group">
											<ul class="wt-attachfile uploaded-placeholder"></ul>
										</div>
										<div class="wt-btnarea">
											<a href="javascript:;" class="wt-btn wt-process-proposal" data-id="<?php echo esc_attr( $current_user->ID ); ?>" data-post="<?php echo esc_attr( $project_id ); ?>"><?php esc_html_e('Send Now', 'workreap'); ?></a>
										</div>	
									</fieldset>
								</div>
							</div>
							<?php wp_nonce_field('workreap_submit_proposal_nounce', 'workreap_submit_proposal_nounce'); ?>
						</form>
					<?php }?>
				</div>
			</div>
		</div>
		<script type="text/template" id="tmpl-load-proposal-docs">
			<li class="wt-uploading attachment-new-item wt-doc-parent" id="thumb-{{data.id}}">
				<span class="uploadprogressbar" style="width:0%"></span>
				<span>{{data.name}}</span>
				<em><?php esc_html_e('File size:', 'workreap'); ?> {{data.size}}<a href="javascript:;" class="lnr lnr-cross wt-remove-attachment"></a></em>
				<input type="hidden" class="attachment_url" name="temp_items[]" value="{{data.url}}">	
			</li>
		</script>	
	</div>
	<?php } else { ?>
	<div class="wt-haslayout wt-main-section">	
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 push-lg-2">
					<div class="wt-jobalertsholder">
						<ul class="wt-jobalerts">
							<li class="alert alert-warning alert-dismissible fade show">
								<span><em><?php esc_html_e('You’re Late:', 'workreap'); ?></em>&nbsp;<?php esc_html_e('We’re sorry but the job you want to apply is no longer available.', 'workreap'); ?></span>
								<a href="javascript:;" class="close" data-dismiss="alert" aria-label="Close"></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }
get_footer();