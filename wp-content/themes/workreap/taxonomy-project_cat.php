<?php
/**
 *
 * The template used for displaying default project category result
 *
 * @package   workreap
 * @author    Amentotech
 * @link      https://themeforest.net/user/amentotech/portfolio
 * @since 1.0
 */
global $wp_query;
get_header();
$archive_show_posts    = get_option('posts_per_page');
$width			= 352;
$height			= 200;
$flag 			= rand(9999, 999999);
$job_option_type	= '';
if( function_exists('fw_get_db_settings_option')  ){
	$job_option_type	= fw_get_db_settings_option('job_option', $default_value = null);
}
?>
<div class="search-result-template wt-haslayout">
	<div class="wt-haslayout wt-cat-section">
		<div class="container">
			<div class="row">
				<div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-8 page-section float-left wt-prjoect-categories">
						<div class="row">
							<?php 
								if( have_posts() ) {
									while ( have_posts() ) : the_post();
									global $post;
									if( $post->post_type === 'projects' ){
										$author_id 		= get_the_author_meta( 'ID' );  
										$linked_profile = workreap_get_linked_profile_id($author_id);
										$employer_title = esc_html( get_the_title( $linked_profile ));	
										$classFeatured	= apply_filters('workreap_project_print_featured', $post->ID,'yes');


										if (function_exists('fw_get_db_post_option')) {
											$db_project_type      = fw_get_db_post_option($post->ID,'project_type');
										}
										?>
										<div class="wt-userlistinghold <?php echo esc_attr($classFeatured);?> wt-userlistingholdvtwo">	
											<div class="wt-userlistingcontent">
												<?php do_action('workreap_project_print_featured', $post->ID); ?>
												<div class="wt-contenthead">
													<div class="wt-title">
														<?php do_action( 'workreap_get_verification_check', $linked_profile, $employer_title ); ?>
														<h2><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php the_title(); ?></a></h2>
													</div>
													<?php do_action( 'workreap_job_short_detail', $post->ID ) ?>
													<div class="wt-description">
														<p><?php echo wp_trim_words( get_the_excerpt(), 30 ); ?></p>
													</div>
													<?php do_action( 'workreap_print_skills_html', $post->ID );?>										
												</div>
												<div class="wt-viewjobholder">
													<ul>
														<?php do_action('workreap_project_print_project_level', $post->ID); ?>
														<?php do_action('workreap_print_project_duration_html', $post->ID);?>
														<?php if(!empty($job_option_type) && $job_option_type === 'enable' ){ do_action('workreap_print_project_option_type', $post->ID); }?>
														<?php do_action('workreap_print_project_type', $post->ID); ?>
														<?php do_action('workreap_print_project_date', $post->ID);?>
														<?php do_action('workreap_print_location', $post->ID); ?>
														<li><?php  do_action('workreap_save_project_html', $post->ID, 'v2'); ?></li>
														<li class="wt-btnarea"><a href="<?php echo esc_url( get_the_permalink() ); ?>" class="wt-btn"><?php esc_html_e( 'View Job', 'workreap' ) ?></a></li>
													</ul>
												</div>
											</div>
										</div>
										<?php } elseif( $post->post_type === 'micro-services' ){ 
											$author_id 				= get_the_author_meta( 'ID' );  
											$linked_profile 		= workreap_get_linked_profile_id($author_id);	
											$service_url			= get_the_permalink();
											$db_docs			= array();
											$delivery_time		= '';
											$order_details		= '';

											if (function_exists('fw_get_db_post_option')) {
												$db_docs   			= fw_get_db_post_option($post->ID,'docs');
												$delivery_time		= fw_get_db_post_option($post->ID,'delivery_time');
												$order_details   	= fw_get_db_post_option($post->ID,'order_details');

											}
										
											if( count( $db_docs )>1 ) {
												$class	= 'wt-freelancers-services-'.intval( $flag ).' owl-carousel';
											} else {
												$class	= '';
											}
											
											if( empty($db_docs) ) {
												$empty_image_class	= 'wt-empty-service-image';
												$is_featured		= workreap_service_print_featured( $post->ID, 'yes');
												$is_featured    	= !empty( $is_featured ) ? 'wt-featured-service' : '';
											} else {
												$empty_image_class	= '';
												$is_featured		= '';
											}

										?>
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 float-left wt-services-grid">
											<div class="wt-freelancers-info <?php echo esc_attr( $empty_image_class );?> <?php echo esc_attr( $is_featured );?>">
												<?php if( !empty( $db_docs ) ) {?>
													<div class="wt-freelancers <?php echo esc_attr( $class );?>">
														<?php
															foreach( $db_docs as $key => $doc ){
																$attachment_id	= !empty( $doc['attachment_id'] ) ? $doc['attachment_id'] : '';
																$thumbnail      = workreap_prepare_image_source($attachment_id, $width, $height);
																if (strpos($thumbnail,'media/default.png') === false) {?>
																	<figure class="item">
																		<a href="<?php echo esc_url( $service_url );?>">
																			<img src="<?php echo esc_url($thumbnail);?>" alt="<?php esc_attr_e('Service ','workreap');?>" class="item">
																		</a>
																	</figure>
															<?php } ?>
														<?php } ?>
													</div>
												<?php } ?>
												<?php do_action('workreap_service_print_featured', $post->ID); ?>
												<?php do_action('workreap_service_shortdescription', $post->ID,$linked_profile); ?>
											</div>
										</div>
									<?php	
									}
								endwhile;
								wp_reset_postdata();
								$qrystr = '';
								if ( $wp_query->found_posts > $archive_show_posts) {?>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-left">
										<div class="theme-nav">
											<?php 
												if (function_exists('workreap_prepare_pagination')) {
													echo workreap_prepare_pagination($wp_query->found_posts , $archive_show_posts);
												}
											?>
										</div>
									</div>
								<?php } ?>
							<?php }?>
						</div>
					</div>
					<aside id="wt-sidebar" class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-4 float-left">
						<div class="wt-sidebar">
							<?php get_sidebar(); ?>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$script	= "jQuery('.wt-freelancers-services-".esc_js($flag)."').owlCarousel({
				items: 1,
				loop:true,
				nav:true,
				margin: 0,
				autoplay:false,
				rtl: ".workreap_owl_rtl_check().",
				navClass: ['wt-prev', 'wt-next'],
				navContainerClass: 'wt-search-slider-nav',
				navText: ['<span class=\"lnr lnr-chevron-left\"></span>', '<span class=\"lnr lnr-chevron-right\"></span>'],
			});";
	wp_add_inline_script( 'workreap-callbacks', $script, 'after' );
get_footer();