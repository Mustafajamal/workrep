"use strict";
var review_pagi		= 2;
var services_pagi	= 2;
var loader_html = '<div class="wt-preloader-section"><div class="wt-preloader-holder"><div class="wt-loader"></div></div></div>';
jQuery(document).on('ready', function() {
    var is_loggedin      = scripts_vars.is_loggedin;
	var user_type      	 = scripts_vars.user_type;
    var wishlist_message = scripts_vars.wishlist_message;
    var service_fee      = parseInt(scripts_vars.service_fee);
    var proposal_message = scripts_vars.proposal_message;
    var proposal_amount  = scripts_vars.proposal_amount;
    var proposal_error   = scripts_vars.proposal_error;
	var proposal_max_val = scripts_vars.proposal_max_val;
    var uploadSize       = scripts_vars.data_size_in_kb;
    var document_title   = scripts_vars.document_title;
	var sys_upload_nonce  	= scripts_vars.sys_upload_nonce; 
	var feature_connects  	= scripts_vars.feature_connects; 
	var connects_pkg  		= scripts_vars.connects_pkg; 
	var hire_service  		= scripts_vars.hire_service; 
	var hire_service_message= scripts_vars.hire_service_message;
	var is_rtl  			= scripts_vars.is_rtl;
	var proposal_price_type = scripts_vars.proposal_price_type;
	var login_register_type = scripts_vars.login_register_type;

    var loader_html = '<div class="wt-preloader-section"><div class="wt-preloader-holder"><div class="wt-loader"></div></div></div>';   
	
	
	jQuery(".wt-numeric, .ca-maxprice, .ca-minprice, .job-cost-input").numeric({ decimal : ".",  negative : false });
	
	jQuery(document).on('click', '.wt-filterholder-three a', function (e) {
		var _this		= jQuery(this);
		jQuery('.wt-order-freelancer').val('');
		jQuery('.wt-order-rating').val('');
		jQuery('.search-freelancersform').submit();
	});
	
	jQuery(document).on('click', '.wt-filterholder-three .wt-tag-radiobox input:radio', function (e) {
		var _this		= jQuery(this);
		var class_name	= _this.attr('id');
		var order_val	= _this.val();
		jQuery('.'+class_name).val(order_val);
		jQuery('.'+class_name).closest('form').submit();

	});
	//collapse filters
	jQuery(document).on('click', '.wtget_url', function (e) {
		var _this 	= jQuery(this);
		var _url	= _this.data('url');
		if( _url ){
			window.location.href = _url;
		}
	});
	
	//collapse filters
	jQuery(document).on('click', '.wt-collapse-filter .wt-widget+.wt-widget .wt-widgettitle', function (e) {
		var _this 			= jQuery(this);
		_this.parents('.wt-widget').toggleClass('wt-displayfilter');
	});
	
	//Remove menu item
	jQuery('li.hide-post-menu').remove();
	
	//filter dropdown
	jQuery(document).on('click', '.custom-price-edit', function (event) {
		var _this = jQuery(this);
		_this.parents('.wt-effectiveholder').find('.offer-filter').toggle();
	});
	
	
	jQuery('.wt-searchgbtn').on('click', function() {
		var _this = jQuery(this);
		
	});
	
	//geo dropdown
	jQuery('.dc-docsearch').on('click', function() {
		var _this = jQuery(this);
		jQuery('.dynamic-column-holder').toggleClass('dynamic-column-holdershow');
	});

	//filter dropdown
	jQuery('.mmobile-floating-apply,.wt-mobile-close').on('click', function() {
		var _this = jQuery(this);
		_this.parents('aside.wt-sidebar').toggleClass('show-mobile-filter');
	});
	
	jQuery(document).on('click', '.geodistance', function (event) {
		event.preventDefault();
		var _this	= jQuery(this);
		_this.next('.geodistance_range').toggle();
	});
	
	/* FIXED SIDEBAR */
	jQuery('.wt-btndemotoggle').on('click', function() {
		var _this = jQuery(this);
		_this.parents('.wt-demo-sidebar').toggleClass('wt-demoshow');
	});
	
	if(jQuery('#wt-verticalscrollbar-demos').length > 0){
		jQuery('#wt-verticalscrollbar-demos').mCustomScrollbar({
			axis:"y",
		});
	}
	
	//Toolip init
	function tipso_init(){
		if(jQuery('.wt-tipso').length > 0){
			jQuery('.wt-tipso').tipso({
				tooltipHover	  : true,
				useTitle		  : false,
				background        : scripts_vars.tip_content_bg,
				titleBackground   : scripts_vars.tip_title_bg,
				color             : scripts_vars.tip_content_color,
				titleColor        : scripts_vars.tip_title_color,
			});
		}
	}
	
	tipso_init();
	
	if( scripts_vars.sticky_header == 'enable' ){
		var winwidth	= jQuery(window).width();
		
		if( winwidth > 768 ){
			var win = jQuery(window);    
			var header = jQuery(".wt-header");
			var headerOffset = header.offset().top || 0;
			var flag = true;
			var triger_once = true;
			jQuery(window).scroll(function() {
				if (win.scrollTop() > headerOffset) {
					if (flag){
						flag = false;
						jQuery("#wt-wrapper").addClass("wt-sticky");
						jQuery(window).trigger('resize').trigger('scroll');
						setTimeout(
						  function() 
						  {
	 						jQuery(window).trigger('resize.px.parallax');
						  }, 300);
					}

				} else {
					if (!flag) {
						flag = true;
						jQuery("#wt-wrapper").removeClass("wt-sticky");
						jQuery(window).trigger('resize').trigger('scroll');
						setTimeout(
						  function() 
						  {
	 						jQuery(window).trigger('resize.px.parallax');
						  }, 300);
					}
				}
			});
		}
	}

	
    //respnsive search from
	jQuery(document).on('click','.wt-search-remove', function($){
		var _this = jQuery(this);
		_this.parents('.wt-search-have').removeClass('show-sform');
	});

	jQuery(document).on('click','.wt-respsonsive-search .wt-searchbtn', function($){
		var _this = jQuery(this);
		_this.parents('.wt-search-have').addClass('show-sform');
	});

	//init chosen
	var config = {
			'.chosen-select'           : {rtl:is_rtl},
			'.chosen-select-deselect'  : {allow_single_deselect:true},
			'.chosen-select-no-single' : {disable_search_threshold:10},
			'.chosen-select-no-results': {no_results_text:scripts_vars.nothing},
			'.chosen-select-width'     : {width:"95%"}
	}

	for (var selector in config) {
		jQuery(selector).chosen(config[selector]);
	}
	
	//send offer
	jQuery(document).on('click','.wt-send-offers', function($){
		var _this = jQuery(this);
		
		if (scripts_vars.user_type == 'employer') {
			jQuery("#chatmodal").modal();
		} else{
			jQuery('.wt-preloader-section').remove();
            jQuery.sticky(scripts_vars.service_access, {classList: 'important', speed: 200, autoclose: 7000});
            return false;
		}
		 
	});
	
	jQuery(document).on('click', '.wt-addons-checkbox', function (e) {
		var _this    		= jQuery(this);
		var _service_id		= _this.data('service-id');
		
		var _checked_vals = []; 
		
		jQuery("input:checkbox[name=addons]:checked").each(function() { 
			_checked_vals.push($(this).data('addons-id')); 
		});
		//alert( _checked_vals );
		jQuery('body').append(loader_html);
		jQuery.ajax({
            type: "POST",
            url: scripts_vars.ajaxurl,
            data: {
				action		 		: 'workreap_service_price_update',
				service_id			: _service_id ,
				addons_ids		 	: _checked_vals
			},
            dataType: "json",
            success: function (response) {
                jQuery('body').find('.wt-preloader-section').remove();
                if (response.type === 'success') {
					jQuery('.wt-ratingtitle h3').html(response.price);
					jQuery('.hire-service').attr('data-addons',_checked_vals);
					jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
                } else {
                    jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
                }
            }
        });
		
	});
	
	//Hire Service Now
    jQuery(document).on('click', '.hire-service', function (e) {
        e.preventDefault();        
        var _this    		= jQuery(this);
        var service_id   	= _this.data('id');
		
		var addons  		= _this.data('addons');
		if (scripts_vars.is_loggedin == 'false') {
			jQuery('.wt-preloader-section').remove();
            jQuery.sticky(scripts_vars.loggedin_message, {classList: 'important', speed: 200, autoclose: 7000});
			jQuery( ".wt-loginbtn, .wt-loginoptionvtwo a" ).trigger( "click" );
			if( login_register_type === 'pages' ){
				jQuery('html, body').animate({scrollTop:0}, 'slow');
			}
            return false;
        }
		
		if (scripts_vars.user_type == 'freelancer') {
			jQuery('.wt-preloader-section').remove();
            jQuery.sticky(scripts_vars.service_access, {classList: 'important', speed: 200, autoclose: 7000});
            return false;
        }

		jQuery.confirm({
			'title': scripts_vars.hire_service,
			'message': scripts_vars.hire_service_message,
			'buttons': {
				'Yes': {
					'class': 'blue',
					'action': function () {
						jQuery('body').append(loader_html);
						jQuery.ajax({
							type: "POST",
							url: scripts_vars.ajaxurl,
							data: {
								action		 	: 'workreap_hire_service',
								addons			: addons,
								service_id		: service_id
							},
							dataType: "json",
							success: function (response) {
								jQuery('body').find('.wt-preloader-section').remove();
								if (response.type === 'success') {
									jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000}); 
									window.location.reload();
								} else if (response.type === 'checkout') {
									jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
									window.location.href = response.checkout_url;
								}else {
									jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
								}
							}
						});
					}
				},
				'No': {
					'class': 'gray',
					'action': function () {
						return false;
					}   // Nothing to do in this case. You can as well omit the action property.
				}
			}
		});
        
    });
	
	//Submit load more reviews
	jQuery(document).on('click', '.load-more-reviews', function(e){
		e.preventDefault(); 
        var _this    	  = jQuery(this);  
		var _author_id    = _this.data('id');
		jQuery('body').append(loader_html);
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: {
				action		 	: 'workreap_get_more_reviews',
				page			: review_pagi,
				author_id		: _author_id
			},
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					review_pagi++;
					jQuery('.review-wrap').append(response.reviews);
				} else {
					jQuery('.load-more-reviews').hide();
				}
			}
		});	
	});
	
	//Submit load more reviews
	jQuery(document).on('click', '.wt-more-rating-service', function(e){
		e.preventDefault(); 
        var _this    	  	= jQuery(this);  
		var _service_id    = _this.data('id');
		jQuery('body').append(loader_html);
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: {
				action		 	: 'workreap_more_rating_service',
				page			: services_pagi,
				service_id		: _service_id
			},
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					services_pagi++;
					jQuery('.wt-reviews').append(response.reviews);
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
					jQuery('.wt-more-reviews').hide();
				}
			}
		});	
	});
	
	//Submit load more servics
	jQuery(document).on('click', '.load-more-services', function(e){
		e.preventDefault(); 
        var _this    	= jQuery(this);  
		var _user_id    = _this.data('id');
		jQuery('body').append(loader_html);
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: {
				action		 	: 'workreap_more_service',
				page			: services_pagi,
				user_id			: _user_id
			},
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					services_pagi++;
					jQuery('.services-wrap').append(response.services);
					jQuery('.wt-freelancers-services-'+response.flag).owlCarousel({
						items: 1,
						loop:true,
						nav:true,
						margin: 0,
						autoplay:false,
						navClass: ['wt-prev', 'wt-next'],
						navContainerClass: 'wt-search-slider-nav',
						navText: ['<span class=\"lnr lnr-chevron-left\"></span>', '<span class=\"lnr lnr-chevron-right\"></span>'],
					});
					
					if( response.show_btn === 'hide' ) {
						jQuery('.more-btn-services').hide();
					}
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
					jQuery('.more-btn-services').hide();
				}
			}
		});	
	});
	
	//MOBILE MENU
	function collapseMenu(){
		jQuery('.wt-navigation ul li.menu-item-has-children, .wt-navigation ul li.page_item_has_children, .wt-navdashboard ul li.menu-item-has-children, .wt-navigation ul li.menu-item-has-mega-menu').prepend('<span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>');
		
		jQuery('.wt-navigation ul li.menu-item-has-children span,.wt-navigation ul li.page_item_has_children span').on('click', function() {
			jQuery(this).parent('li').toggleClass('wt-open');
			jQuery(this).next().next().slideToggle(300);
		});
		
		jQuery('.wt-navigation ul li.menu-item-has-children > a, .wt-navigation ul li.page_item_has_children > a').on('click', function() {
			if ( location.href.indexOf("#") != -1 ) {
				jQuery(this).parent('li').toggleClass('wt-open');
				jQuery(this).next().slideToggle(300);
			} else{
				//do nothing
			}
			
		});
		
		jQuery('.wt-navdashboard > ul > li.menu-item-has-children > a').on('click', function() {
			jQuery(this).parents('.menu-item-has-children').toggleClass('wt-open');
			jQuery(this).parents('.menu-item-has-children').find('.sub-menu').slideToggle(300);
		});
		
	}
	collapseMenu();	
	
	//Auto adjust menu item for sub childs
	const loginJs 		= document.querySelector(".wt-userlogedin");
	if ( loginJs === null ){
		for(var i = 1; i<4; i++){
			jQuery('.nav-Js > li:nth-last-child('+i+') ul').css("left", "auto");
			jQuery('.nav-Js > li:nth-last-child('+i+') ul').css("right", "0");
			jQuery('body.rtl .nav-Js > li:nth-last-child('+i+') ul').css("left", "0");
			jQuery('body.rtl .nav-Js > li:nth-last-child('+i+') ul').css("right", "auto");
		} 
		for(var i =1; i<5; i++ ){
			jQuery('.wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("left", "auto").addClass('menu-item-moved');
			jQuery('.wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("right", "100%");
			jQuery('body.rtl .wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, body.rtl .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("right", "auto").addClass('menu-item-moved');
			jQuery('body.rtl .wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, body.rtl .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("left", "100%");
		}
	}else{
	  	for( var i = 1; i<3; i++ ){
	  		jQuery('.wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("left", "auto").addClass('menu-item-moved');
	  		jQuery('.wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("right", "100%");
	  		jQuery('body.rtl .wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, body.rtl .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("left", "100%").addClass('menu-item-moved');
	  		jQuery('body.rtl .wt-navigation > ul > li.menu-item-has-children:nth-last-child('+i+') .sub-menu li .sub-menu, body.rtl .wt-navigation > ul > li.page_item_has_children:nth-last-child('+i+') .children li .children').css("right", "auto");
	  	}
	}
	
	//Filter search fields
	jQuery.expr[':'].contains = function(a, i, m) {
		return jQuery(a).text().toUpperCase()
		  .indexOf(m[3].toUpperCase()) >= 0;
	};

	//Apply contians filter
	jQuery('.wt-filter-field').on('keyup', function($){
		var content = jQuery(this).val();        
		jQuery(this).parents('fieldset').siblings('fieldset').find('.wt-checkbox:contains(' + content + ')').show();
		jQuery(this).parents('fieldset').siblings('fieldset').find('.wt-checkbox:not(:contains(' + content + '))').hide();                 
	}); 
	
	//read more skills
    jQuery(document).on('click', '.showmore_skills', function (e) {
        e.preventDefault();
		var _this	= jQuery(this);
		var id		= _this.data('id');
		jQuery(_this).hide();
		jQuery('.skills_'+id).css('display','');
    });
	
	//read more crafted projects
    jQuery(document).on('click', '.wt-loadmore-crprojects', function (e) {
        e.preventDefault();
		jQuery('.wt-crprojects').css('display','block');
		jQuery('.wt-loadmore-crprojects').css('display','none');
    });
	
	//read more crafted videos
    jQuery(document).on('click', '.wt-loadmore-videos', function (e) {
        e.preventDefault();
		jQuery('.wt-video-list').css('display','block');
		jQuery('.wt-loadmore-videos').css('display','none');
    });
	
	//GET QR Code
    jQuery(document).on('click', '.tg-qrcodedetails', function (e) {
        e.preventDefault();
        jQuery('body').append(loader_html);
		var _this = jQuery(this);
		var id = _this.data('key'); 
		var type = _this.data('type');  
		var dataString = 'key=' + id + '&type=' + type + '&action=workreap_generate_qr_code';   
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {  
					jQuery('.tg-qrcodedata').attr('src', response.key);
					jQuery('.tg-qrscan figcaption').remove();
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
				}
			}
		});
    });
	
	//follow employer
    jQuery(document).on('click', '.wt-follow-emp', function (e) {
        e.preventDefault();
        jQuery('body').append(loader_html);
		var _this = jQuery(this); 
		var _type = _this.data('type');  
		var _id   = _this.data('id');  
		var _text = _this.data('text');  
		var dataString = 'type=' + _type + '&id=' + _id + '&action=workreap_follow_employer';   
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					_this.removeClass('wt-follow-emp');
					_this.addClass('wt-clicksave');
					_this.find('i').addClass('fa');
					_this.find('span').html(_text);
					
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
				}
			}
		});
    });
	
	//follow freelancer
    jQuery(document).on('click', '.wt-savefreelancer', function (e) {
        e.preventDefault();
        jQuery('body').append(loader_html);
		var _this = jQuery(this);  
		var _id   = _this.data('id');  
		var _text = _this.data('text');  
		var dataString = 'id=' + _id + '&action=workreap_follow_freelancer';   
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					_this.removeClass('wt-savefreelancer');
					_this.find('i').addClass('fa');
					_this.find('span').html(_text);
					
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
				}
			}
		});
    });
	
	//Add to saved projects  
    jQuery(document).on('click', '.wt-add-to-saved_projects', function (e) { 
        e.preventDefault();
		jQuery('body').append(loader_html);
        
		if (scripts_vars.is_loggedin == 'false') {
			jQuery('.wt-preloader-section').remove();
            jQuery.sticky(scripts_vars.wishlist_message, {classList: 'important', speed: 200, autoclose: 7000});
            return false;
        }
		
        var _this 	= jQuery(this);
        var id 		= _this.data('id') ;    
        var dataString = 'project_id=' + id + '&action=workreap_add_project_to_wishlist';
		
        jQuery.ajax({
            type: "POST",
            url: scripts_vars.ajaxurl,
            data: dataString,
            dataType: "json",
            success: function (response) {
               jQuery('.wt-preloader-section').remove();
               if (response.type === 'success') {
                    _this.removeClass('wt-add-to-saved_projects');
                    _this.addClass('wt-clicksave');
                    _this.find('em').html( response.text );
                    jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000 });                   
                } else {
                    jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
                }
            }
        });           
    });
	
	//follow service
    jQuery(document).on('click', '.wt-saveservice', function (e) {
		
        e.preventDefault();
        jQuery('body').append(loader_html);
		var _this = jQuery(this);  
		var _id   = _this.data('id');  
		var _text = _this.data('text');  
		var dataString = 'id=' + _id + '&action=workreap_follow_service';   
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					_this.removeClass('wt-saveservice');
					_this.find('i').addClass('fa');
					_this.find('span').html(_text);
					jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
				}
			}
		});
    });
	
	//report user
    jQuery(document).on('click', '.wt-report-user', function (e) {
        e.preventDefault();
        jQuery('body').append(loader_html);
		var _this 	= jQuery(this); 
		var _type 	= _this.data('type');  
		var _id   	= _this.data('id'); 
		var _form	= _this.parents('.wt-formreport').serialize();
		var dataString = 'type=' + _type + '&id=' + _id + '&'+_form+'&action=workreap_report_user';   
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType: "json",
			success: function (response) {
				jQuery('body').find('.wt-preloader-section').remove();
				if (response.type === 'success') {
					jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
					jQuery('.wt-formreport').get(0).reset();
				} else {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
					if (response.loggin === 'false') {
						jQuery( ".wt-loginbtn, .wt-loginoptionvtwo a" ).trigger( "click" );
						if( login_register_type === 'pages' ){
							jQuery('html, body').animate({scrollTop:0}, 'slow');
						}
					}
				}
			}
		});
    });	
	
	//skill bar
	try {
		jQuery('#wt-ourskill').appear(function () {
			jQuery('.wt-skillholder').each(function () {
				jQuery(this).find('.wt-skillbar').animate({
					width: jQuery(this).attr('data-percent')
				}, 2500);
			});
		});
	} catch (err) {}
	
	
	//Load More Options
	_show_hide_list('.items-more-wrap-sk');
	_show_hide_list('.items-more-wrap-pr');
	_show_hide_list('.items-more-wrap-aw');
	_show_hide_list('.items-more-wrap-ed');
	_show_hide_list('.items-more-wrap-ex');
	var items_wrap = '.subcat-search-wrap';
	function _show_hide_list(items_wrap) {
		var size_li = jQuery(items_wrap + " .data-list .sp-load-item").size();
		var x = 10;
		
		jQuery(items_wrap + ' .data-list .sp-load-item:lt(' + x + ')').show();
		
		jQuery(document).on('click', items_wrap + ' .sp-loadMore', function(){
			x = (x + 10 <= size_li) ? x + 10 : size_li;
			jQuery(items_wrap + ' .data-list .sp-load-item:lt(' + x + ')').addClass('sp-disply');
			jQuery(items_wrap + ' .data-list .sp-load-item:lt(' + x + ')').show();
			var disply_size = jQuery(items_wrap + " .data-list .sp-disply").size();
			if( disply_size >= size_li ) {
				jQuery(items_wrap + " .sp-loadMore").hide();
			}
		});
		
		jQuery(document).on('click',items_wrap + ' sp-showLess', function(){
			x = (x - 10 < 0) ? 10 : x - 10;
			jQuery(items_wrap + ' .data-list .sp-load-item').not(':lt(' + x + ')').hide();
		});
		
		if (size_li <= 10) {
			jQuery(items_wrap + " .sp-loadMore").hide();
		}
	}
	
    //Proposal Hourly amount
    jQuery('input.wt-estimated-hours').on('keyup',function(e){
        var TotalAmount = jQuery(this).val();
		var hourlytime	= jQuery('.wt-proposal-amount').val();

        if( isNaN( TotalAmount ) ){
            jQuery.sticky(proposal_amount, {classList: 'important', speed: 200, autoclose: 5000});
            jQuery(this).focus();
            return false;
        } else { 
			TotalAmount		= TotalAmount * hourlytime;
            var commision   = service_fee;        
            var amount      = ( TotalAmount / 100 ) * commision;
            amount          = amount.toFixed(2);
            var userAmount = TotalAmount - amount;
            
			if( userAmount == 0 ){
                userAmount = '00.00';
            }
			
            jQuery( 'em.wt-service-fee' ).text('-' + amount); 
            jQuery( '.wt-user-amount' ).text(userAmount); 
			jQuery( 'em.wt-project-proposed' ).text(TotalAmount); 
        }
        //Some mechanism for only integer valus needed   
    });
	
	//Proposal amount
    jQuery('input.wt-proposal-amount').on('keyup',function(e){
        var TotalAmount = jQuery(this).val();
		var hourlytime	= jQuery('.wt-estimated-hours').val();
		
		if( hourlytime == 'undefined' || hourlytime == null ){
		} else {
			TotalAmount	= TotalAmount * hourlytime ;
		}
		
        if( isNaN( TotalAmount ) ){
            jQuery.sticky(proposal_amount, {classList: 'important', speed: 200, autoclose: 5000});
            jQuery(this).focus();
            return false;
        } else {            
            var commision   = service_fee;        
            var amount      = ( TotalAmount / 100 ) * commision;
            amount          = amount.toFixed(2);
            var userAmount = TotalAmount - amount;
            if( userAmount == 0 ){
                userAmount = '00.00';
            }
			
            jQuery( 'em.wt-service-fee' ).text('-' + amount); 
            jQuery( '.wt-user-amount' ).text(userAmount); 
			jQuery( 'em.wt-project-proposed' ).text(TotalAmount); 
        }
        //Some mechanism for only integer valus needed   
    });
    
	//Job add upload attachment
	var ProposalUploaderArguments = {
		browse_button: 'proposal-btn', // this can be an id of a DOM element or the DOM element itself
		file_data_name: 'file_name',
		container: 'wt-proposal-container',
		drop_element: 'proposal-drag',
		multipart_params: {
			"type": "file_name",
		},
		multi_selection: true,
		//chunk_size: 100,
		url: scripts_vars.ajaxurl + "?action=workreap_temp_file_uploader&nonce=" + sys_upload_nonce,
		filters: {
			mime_types: [
				{title: scripts_vars.proposal_attachments, extensions: "pdf,doc,docx,xls,xlsx,ppt,pptx,csv,jpg,jpeg,gif,png,zip,rar"}
			],
			max_file_size: uploadSize,
			max_file_count: 1,
			prevent_duplicates: false
		}
	};

	var ProposalUploader = new plupload.Uploader(ProposalUploaderArguments);
	ProposalUploader.init();

	//bind
	ProposalUploader.bind('FilesAdded', function (up, files) {
		var _Thumb = "";
		
		plupload.each(files, function (file) {
			var load_thumb = wp.template('load-proposal-docs');
			var _size 	= bytesToSize(file.size);
            var data 	= {id: file.id,size:_size,name:file.name,percentage:file.percent};       
            load_thumb  = load_thumb(data);
            _Thumb 		+= load_thumb;
		});

		jQuery('.wt-formprojectinfo .uploaded-placeholder').append(_Thumb);
		jQuery('.wt-formprojectinfo .uploaded-placeholder').addClass('wt-infouploading');
		up.refresh();
		ProposalUploader.start();
	});

	//bind
	ProposalUploader.bind('UploadProgress', function (up, file) {
		var _html = '<span class="uploadprogressbar" style="width:'+file.percent+'%"></span>';
        jQuery('.wt-formprojectinfo .uploadprogressbar').replaceWith(_html);
	});

	//Error
	ProposalUploader.bind('Error', function (up, err) {
		jQuery.sticky(err.message, {classList: 'important', speed: 200, autoclose: 5000});
	});

	//display data
	ProposalUploader.bind('FileUploaded', function (up, file, ajax_response) {

		var response = $.parseJSON(ajax_response.response);
		if ( response.type === 'success' ) {
			jQuery('#thumb-'+file.id).removeClass('wt-uploading');
			jQuery('#thumb-'+file.id +' .attachment_url').val(response.thumbnail);
		} else {
			jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
		}
	});

	//Delete Award Image
	jQuery(document).on('click', '.wt-remove-attachment', function (e) {
		e.preventDefault();
		var _this = jQuery(this);
		_this.parents('.wt-doc-parent').remove();
	});
	
	
    //Init Plupupload for proposal documents
    if (jQuery(".wt-formproposal").length) {              
        var uploaderDocumentArguments = {
            browse_button: 'wt-upload-doc', 
            file_data_name: 'file_name',
            container: 'wt-attachfile',
            drop_element: 'wt-attachfile',
            multipart_params: {
                "type": "file_name",
            },
            url: scripts_vars.ajaxurl + "?action=workreap_temp_file_uploader",
            filters: {
                mime_types: [
                    {title: document_title, extensions: "pdf,doc,docx,xls,xlsx,ppt,pptx,csv,jpg,jpeg,gif,png,zip,rar"}
                ],
                max_file_size: uploadSize,
                prevent_duplicates: false
            }
        };
        
        //uploader init
        var proposalDocumentUploader = new plupload.Uploader(uploaderDocumentArguments);
        proposalDocumentUploader.init();
        //Bind
        proposalDocumentUploader.bind('FilesAdded', function (up, files) {
            var _thumb = "";
            plupload.each(files, function (file) {
                _thumb += '<div class="tg-galleryimg ad-item ad-thumb-item" id="thumb-' + file.id + '">' + '' + '</div>';                
            });

            jQuery('.sp-profile-ad-photos .tg-galleryimages').append(_thumb);
            up.refresh();
            proposalDocumentUploader.start();
        });

        //Bind
        proposalDocumentUploader.bind('UploadProgress', function (up, file) {
            jQuery('body').append(loader_html);            
        });    

        //Error
        proposalDocumentUploader.bind('Error', function (up, err) {
            jQuery.sticky(err.message, {classList: 'important',position:'top-right', speed: 200, autoclose: 5000});
        });


        //display data
        proposalDocumentUploader.bind('FileUploaded', function (up, file, ajax_response) {
            jQuery('body').find('.wt-preloader-section').remove();
            var response = $.parseJSON(ajax_response.response);
            if (response.type === 'success') {
                var load_proposal_docs = wp.template('load-proposal-docs');       
                var data = {name: response.name, url:response.thumbnail, size:response.size};        
                load_proposal_docs = load_proposal_docs(data);                      
                jQuery('#wt-attachfile').append(load_proposal_docs);                               
            } else {
                jQuery.sticky(response.message, {classList: 'important',position:'top-right', speed: 200, autoclose: 5000});
                jQuery("#thumb-" + file.id).remove();
            }
                 
        });
    
    }

    //Delete proposal files
    jQuery(document).on('click','.wt-remove-proposal-doc', function(){        
        jQuery(this).parents('.wt-doc-parent').remove();
    });
   
    //Send proposal
    jQuery(document).on('click', '.wt-process-proposal', function (e) {
        e.preventDefault();        
        var _this    			= jQuery(this);         
        var _id      			= parseInt(_this.data('id'));
        var _post_id 			= parseInt(_this.data('post'));
		var proposalLink 		= jQuery(this).attr('href');
		var max_val				= parseInt(jQuery('.wt-proposal-amount').attr('max'));
		var p_amount			= parseInt(jQuery('.wt-proposal-amount').val());
		
        if (is_loggedin == 'false') {
            jQuery.sticky(proposal_error, {classList: 'important', speed: 200, autoclose: 7000});
            return false;
        } 
		
		if(feature_connects === false) {
			jQuery.sticky(connects_pkg, {classList: 'important', speed: 200, autoclose: 5000});
            return false;
		}
		
        if( _id == '' || _id == 0) {
            jQuery.sticky(proposal_message, {classList: 'important', speed: 200, autoclose: 5000});
            return false;
        }
		
		if( proposal_price_type === 'budget' ){
			if ( p_amount > max_val) {
				jQuery.sticky(proposal_max_val, {classList: 'important', speed: 200, autoclose: 7000});
				return false;
			} 
		}

        jQuery('body').append(loader_html);
        var _form   = _this.parents('.wt-send-project-proposal').serialize();
        var dataString = '&post_id=' + _post_id + '&id=' + _id + '&'+_form+'&action=workreap_process_project_proposal';   
		
        jQuery.ajax({
            type: "POST",
            url: scripts_vars.ajaxurl,
            data: dataString,
            dataType: "json",
            success: function (response) {
                jQuery('body').find('.wt-preloader-section').remove();
				
                if (response.type === 'success') {
                    jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
                    window.location.href = response.return;
                } else {
                    jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
                }
            }
        });
    });            
	
    //Send proposal error
    jQuery('.wt-submit-proposal').on('click', function(e){
        e.preventDefault();
        var proposalLink = jQuery(this).attr('href');
        if (is_loggedin == 'false') {
            jQuery.sticky(proposal_error, {classList: 'important', speed: 200, autoclose: 7000});
            return false;
        } else {
            window.location.href = proposalLink;
        }
    });

	//scrollbar
	if(jQuery('.wt-filterscroll').length > 0){
		jQuery('.wt-filterscroll').mCustomScrollbar({
			axis:"y",
		});
	} 
	//OPEN CLOSE
	jQuery('#wt-loginbtn, .wt-loginheader a').on('click', function(event){
		event.preventDefault();
		jQuery('.wt-loginarea .wt-loginformhold').slideToggle();
	});
	
	//OPEN CLOSE
	jQuery('.wt-loginfor-offer').on('click', function(event){
		event.preventDefault();
		jQuery( ".wt-loginoptionvtwo a" ).trigger( "click" );
		if( login_register_type === 'pages' ){
			jQuery('html, body').animate({scrollTop:0}, 'slow');
		}
	});
	
	//OPEN CLOSE
	jQuery('.wt-dropdown').on('click', function(event){
		event.preventDefault();
		var _this = jQuery(this);
		_this.parents('.wt-formbanner').find('.wt-radioholder').slideToggle();
	});	
	
	//DROPDOWN RADIO
	jQuery('input:radio[name="searchtype"]').on('change',function(){
			var _this = jQuery(this);
	        var _type = _this.data('title');
			var _url  = _this.data('url');
			
			jQuery('.wt-formbanner').attr('action', _url);
	        _this.parents('.wt-formbanner').find('.selected-search-type').html(_type);
			_this.parents('.wt-formbanner').find('.wt-radioholder').slideToggle();
			
	    }
    );
	
	//DROPDOWN select
	jQuery('select[name="searchtype"]').on('change',function(){
			var _this = jQuery(this);
			var _url  = _this.find(':selected').data('url');
			_this.parents('.do-append-url').attr('action', _url);
	    }
    );

    //Slider
    if( jQuery("#wt-productrangeslider").length > 0 ){
        ageRangeslider();
    }

    //Switch hourly/fixed type
    jQuery('.wt-type').change(function (event) {
        var value = jQuery(this).val();
        if( value == 'hourly' ){
            jQuery('#wt-productrangeslider').removeClass('wt-none');
            jQuery('.wt-amountbox').removeClass('wt-none');
        } else {
            jQuery('#wt-productrangeslider').addClass('wt-none');
            jQuery('.wt-amountbox').addClass('wt-none');
        } 
    });
	
	//login POP
	jQuery('wt-searchbtn').on('click', function(event){
		event.preventDefault();
		jQuery('.wt-loginarea .wt-loginformhold').slideToggle();
	});
	
	// for prety vido popup
	jQuery("a[data-rel]").each(function () {
		jQuery(this).attr("rel", jQuery(this).data("rel"));
	});
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
		animation_speed: 'normal',
		theme: 'dark_square',
		slideshow: 3000,
		default_width: 800,
        default_height: 500,
        allowfullscreen: true,
		autoplay_slideshow: false,	
		social_tools: false,
		iframe_markup: "<iframe src='{path}' width='{width}' height='{height}' frameborder='no' allowfullscreen='true'></iframe>", 
		deeplinking: false
	});
	
	/*OPEN CLOSE */
	jQuery('.wt-headerbtn').on('click', function(event){
		event.preventDefault();
		jQuery('.wt-headersearch .wt-loginformhold').slideToggle();
	});
	
});

/*
 Sticky v2.1.2 by Andy Matthews
 http://twitter.com/commadelimited
 
 forked from Sticky by Daniel Raftery
 http://twitter.com/ThrivingKings
 */
(function ($) {

    $.sticky = $.fn.sticky = function (note, options, callback) {

        // allow options to be ignored, and callback to be second argument
        if (typeof options === 'function')
            callback = options;

        // generate unique ID based on the hash of the note.
        var hashCode = function (str) {
            var hash = 0,
                    i = 0,
                    c = '',
                    len = str.length;
            if (len === 0)
                return hash;
            for (i = 0; i < len; i++) {
                c = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + c;
                hash &= hash;
            }
            return 's' + Math.abs(hash);
        },
                o = {
                    position: 'top-right', // top-left, top-right, bottom-left, or bottom-right
                    speed: 'fast', // animations: fast, slow, or integer
                    allowdupes: true, // true or false
                    autoclose: 5000, // delay in milliseconds. Set to 0 to remain open.
                    classList: '' // arbitrary list of classes. Suggestions: success, warning, important, or info. Defaults to ''.
                },
        uniqID = hashCode(note), // a relatively unique ID
                display = true,
                duplicate = false,
                //tmpl = '<div class="tg-alertmessage" id="ID"><div class="sticky border-POS"><em class="lnr lnr-bullhorn CLASSLIST"></em><span class="sticky-close"></span><p class="sticky-note">NOTE</p></div></div>',
                tmpl = '<div id="ID" class="jf-alert alert-dismissible border-POS CLASSLIST" role="alert"><button type="button" class="jf-close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="lnr lnr-cross"></i></span></button><div class="jf-description"><p><i class="lnr lnr-bullhorn"></i>NOTE</p></div></div>',
                positions = ['top-right', 'top-center', 'top-left', 'bottom-right', 'bottom-center', 'bottom-left','middle-left','middle-right','middle-center'];

        // merge default and incoming options
        if (options)
            $.extend(o, options);

        // Handling duplicate notes and IDs
        $('.sticky').each(function () {
            if ($(this).attr('id') === hashCode(note)) {
                duplicate = true;
                if (!o.allowdupes)
                    display = false;
            }
            if ($(this).attr('id') === uniqID)
                uniqID = hashCode(note);
        });
        
        if( scripts_vars.sm_success ){
            var _position   = scripts_vars.sm_success;
        } else{
            var _position   = o.position;
        }
        
        // Make sure the sticky queue exists
        if (!$('.sticky-queue').length) {
            $('body').append('<div class="sticky-queue ' + _position + '">');
        } else {
            // if it exists already, but the position param is different,
            // then allow it to be overridden
            $('.sticky-queue').removeClass(positions.join(' ')).addClass(_position);
        }

        // Can it be displayed?
        if (display) {
            // Building and inserting sticky note
            $('.sticky-queue').prepend(
                    tmpl
                    .replace('POS', _position)
                    .replace('ID', uniqID)
                    .replace('NOTE', note)
                    .replace('CLASSLIST', o.classList)
                    ).find('#' + uniqID)
                    .slideDown(o.speed, function () {
                        display = true;
                        // Callback function?
                        if (callback && typeof callback === 'function') {
                            callback({
                                'id': uniqID,
                                'duplicate': duplicate,
                                'displayed': display
                            });
                        }
                    });

        }

        // Listeners
        $('.sticky').ready(function () {
            // If 'autoclose' is enabled, set a timer to close the sticky
            if (o.autoclose) {
                $('#' + uniqID).delay(o.autoclose).fadeOut(o.speed, function () {
                    // remove element from DOM
                    $(this).remove();
                });
            }
        });

        // Closing a sticky
        $('.jf-close').on('click', function () {
            var _this   = $(this);
            
            if( _this.parents('.jf-alert').hasClass('sp-cacheit') ){
                jQuery.confirm({
                    'title': scripts_vars.cache_title,
                    'message': scripts_vars.cache_message,
                    'buttons': {
                        'Yes': {
                            'class': 'blue',
                            'action': function () {

                                if( _this.parents('.jf-alert').hasClass('cache-verification') ){
                                    $.cookie('sp_cache_verification_'+scripts_vars.current_user_id, 'true', { expires: 365 });
                                } else if( _this.parents('.jf-alert').hasClass('cache-deactivation') ){
                                    $.cookie('sp_cache_deactivation_'+scripts_vars.current_user_id, 'true', { expires: 365 });
                                }

                                //Remove message
                                $('#' + _this.parents('.jf-alert').attr('id')).dequeue().fadeOut(o.speed, function () {
                                    // remove element from DOM
                                    _this.remove();
                                });

                            }
                        },
                        'No': {
                            'class': 'gray',
                            'action': function () {
                                return false;
                            }   // Nothing to do in this case. You can as well omit the action property.
                        }
                    }
                });
            } else{
                //Remove message
                $('#' + _this.parents('.jf-alert').attr('id')).dequeue().fadeOut(o.speed, function () {
                    // remove element from DOM
                    _this.remove();
                });
            }
        });

    };
})(jQuery);
           
/**Mega Menu*/
jQuery(function ($) {

    function hoverIn() {
        var a = $(this);
        var nav = a.closest('.wt-navigation');
        var mega = a.find('.mega-menu');
        var offset = rightSide(nav) - leftSide(a);
        var winwidth	= jQuery(window).width();
		if( winwidth > 768 ){
			mega.width(Math.min(rightSide(nav), columns(mega) * 325));
			mega.css('left', Math.min(0, offset - mega.width()));
			mega.fadeIn('fast').css("display","block");
		}
		
    }

    function hoverOut() {
		var winwidth	= jQuery(window).width();
		if( winwidth > 768 ){
			var a = $(this);
			var nav = a.closest('.wt-navigation');
			var mega = a.find('.mega-menu');
			mega.fadeOut('fast').css("display","none");
		}
    }

    function columns(mega) {
        var columns = 0;
        mega.children('.mega-menu-row').each(function () {
            columns = Math.max(columns, $(this).children('.mega-menu-col').length);
        });
        return columns;
    }

    function leftSide(elem) {
        return elem.offset().left;
    }

    function rightSide(elem) {
        return elem.offset().left + elem.width();
    }
	
	var winwidth	= jQuery(window).width();
	jQuery('.wt-navigation .menu-item-has-mega-menu').hover(hoverIn, hoverOut);
});

//Preloader
jQuery(window).load(function () {
	var loading_duration = scripts_vars.loading_duration;
    jQuery(".preloader-outer").delay(loading_duration).fadeOut();
    jQuery(".pins").delay(loading_duration).fadeOut("slow");
});

// Confirm Box
(function ($) {

    jQuery.confirm = function (params) {

        if ($('#confirmOverlay').length) {
            // A confirm is already shown on the page:
            return false;
        }

        var buttonHTML = '';
        $.each(params.buttons, function (name, obj) {

			// Generating the markup for the buttons:
			if( name == 'Yes' ){
				name	= scripts_vars.yes;
			} else if( name == 'No' ){
				name	= scripts_vars.no;
			} else{
				name	= name;
			}
			
            buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';
            if (!obj.action) {
                obj.action = function () {
                };
            }
        });
        var markup = [
            '<div id="confirmOverlay">',
            '<div id="confirmBox">',
            '<h1>', params.title, '</h1>',
            '<p>', params.message, '</p>',
            '<div id="confirmButtons">',
            buttonHTML,
            '</div></div></div>'
        ].join('');
        $(markup).hide().appendTo('body').fadeIn();
        var buttons = $('#confirmBox .button'),
                i = 0;
        $.each(params.buttons, function (name, obj) {
            buttons.eq(i++).on('click',function () {
                obj.action();
                jQuery.confirm.hide();
                return false;
            });
        });
    }

    jQuery.confirm.hide = function () {
        $('#confirmOverlay').fadeOut(function () {
            $(this).remove();
        });
    }

})(jQuery);

//get distance
function _get_distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

// get rounded value
function _get_round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

// string replace URL
function _string_replace_url(url) {
	return url;
}

//Map styles
function workreap_get_map_styles(style) {

    var styles = '';
    if (style == 'view_1') {
        var styles = [{"featureType": "administrative.country", "elementType": "geometry", "stylers": [{"visibility": "simplified"}, {"hue": "#ff0000"}]}];
    } else if (style == 'view_2') {
        var styles = [{"featureType": "water", "elementType": "all", "stylers": [{"hue": "#7fc8ed"}, {"saturation": 55}, {"lightness": -6}, {"visibility": "on"}]}, {"featureType": "water", "elementType": "labels", "stylers": [{"hue": "#7fc8ed"}, {"saturation": 55}, {"lightness": -6}, {"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"hue": "#83cead"}, {"saturation": 1}, {"lightness": -15}, {"visibility": "on"}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"hue": "#f3f4f4"}, {"saturation": -84}, {"lightness": 59}, {"visibility": "on"}]}, {"featureType": "landscape", "elementType": "labels", "stylers": [{"hue": "#ffffff"}, {"saturation": -100}, {"lightness": 100}, {"visibility": "off"}]}, {"featureType": "road", "elementType": "geometry", "stylers": [{"hue": "#ffffff"}, {"saturation": -100}, {"lightness": 100}, {"visibility": "on"}]}, {"featureType": "road", "elementType": "labels", "stylers": [{"hue": "#bbbbbb"}, {"saturation": -100}, {"lightness": 26}, {"visibility": "on"}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"hue": "#ffcc00"}, {"saturation": 100}, {"lightness": -35}, {"visibility": "simplified"}]}, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"hue": "#ffcc00"}, {"saturation": 100}, {"lightness": -22}, {"visibility": "on"}]}, {"featureType": "poi.school", "elementType": "all", "stylers": [{"hue": "#d7e4e4"}, {"saturation": -60}, {"lightness": 23}, {"visibility": "on"}]}];
    } else if (style == 'view_3') {
        var styles = [{"featureType": "water", "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]}, {"featureType": "road", "elementType": "geometry.fill", "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]}, {"featureType": "road", "elementType": "geometry.stroke", "stylers": [{"color": "#808080"}, {"lightness": 54}]}, {"featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [{"color": "#ece2d9"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#ccdca1"}]}, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#767676"}]}, {"featureType": "road", "elementType": "labels.text.stroke", "stylers": [{"color": "#ffffff"}]}, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {"featureType": "landscape.natural", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]}, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {"featureType": "poi.sports_complex", "stylers": [{"visibility": "on"}]}, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {"featureType": "poi.business", "stylers": [{"visibility": "simplified"}]}];
    } else if (style == 'view_4') {
        var styles = [{"elementType": "geometry", "stylers": [{"hue": "#ff4400"}, {"saturation": -68}, {"lightness": -4}, {"gamma": 0.72}]}, {"featureType": "road", "elementType": "labels.icon"}, {"featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{"hue": "#0077ff"}, {"gamma": 3.1}]}, {"featureType": "water", "stylers": [{"hue": "#00ccff"}, {"gamma": 0.44}, {"saturation": -33}]}, {"featureType": "poi.park", "stylers": [{"hue": "#44ff00"}, {"saturation": -23}]}, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"hue": "#007fff"}, {"gamma": 0.77}, {"saturation": 65}, {"lightness": 99}]}, {"featureType": "water", "elementType": "labels.text.stroke", "stylers": [{"gamma": 0.11}, {"weight": 5.6}, {"saturation": 99}, {"hue": "#0091ff"}, {"lightness": -86}]}, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"lightness": -48}, {"hue": "#ff5e00"}, {"gamma": 1.2}, {"saturation": -23}]}, {"featureType": "transit", "elementType": "labels.text.stroke", "stylers": [{"saturation": -64}, {"hue": "#ff9100"}, {"lightness": 16}, {"gamma": 0.47}, {"weight": 2.7}]}];
    } else if (style == 'view_5') {
        var styles = [{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 21}]}, {"elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}];
    } else if (style == 'view_6') {
        var styles = [{"featureType": "landscape", "stylers": [{"hue": "#FFBB00"}, {"saturation": 43.400000000000006}, {"lightness": 37.599999999999994}, {"gamma": 1}]}, {"featureType": "road.highway", "stylers": [{"hue": "#FFC200"}, {"saturation": -61.8}, {"lightness": 45.599999999999994}, {"gamma": 1}]}, {"featureType": "road.arterial", "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 51.19999999999999}, {"gamma": 1}]}, {"featureType": "road.local", "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 52}, {"gamma": 1}]}, {"featureType": "water", "stylers": [{"hue": "#0078FF"}, {"saturation": -13.200000000000003}, {"lightness": 2.4000000000000057}, {"gamma": 1}]}, {"featureType": "poi", "stylers": [{"hue": "#00FF6A"}, {"saturation": -1.0989010989011234}, {"lightness": 11.200000000000017}, {"gamma": 1}]}];
    } else {
        var styles = [{"featureType": "administrative.country", "elementType": "geometry", "stylers": [{"visibility": "simplified"}, {"hue": "#ff0000"}]}];
    }
    return styles;
}


//convert bytes to KB< MB,GB,TB
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

//validate amount
function validateAmount(_this) {
    if (isNaN(jQuery.trim(jQuery(_this).val()))) {
        jQuery(_this).val("");
    } else {
        var amt = jQuery(_this).val();
        if (amt != '') {
            if (amt.length > 16) {
                amt = amt.substr(0, 16);
                jQuery(_this).val(amt);
            }
            //amount = amt;
            return true;
        } else {
            //amount = gloAmount;
            return true;
        }
    }
}

//get random ID
function get_random_number() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4();
}

// Project Range Slider
function ageRangeslider(){
    var minVal = jQuery('#wt-consultationfeeamount').data('min');
    var maxVal = jQuery('#wt-consultationfeeamount').data('max');
    jQuery("#wt-productrangeslider").slider({
        range: true,
        min: 0,
        max: 150,
        values: [ minVal, maxVal ],
        slide: function( event, ui ) {
            jQuery( "#wt-consultationfeeamount" ).val( "$" + ui.values[ 0 ] + "- $" + ui.values[ 1 ] );
        }
    });
    jQuery( "#wt-consultationfeeamount" ).val( "$" + jQuery("#wt-productrangeslider").slider( "values", 0 ) + " - $" + jQuery("#wt-productrangeslider").slider( "values", 1 ));
}

// Init Full Width Sections
builder_full_width_section(); //Init Sections
var $ = window.jQuery;
$(window).off("resize.sectionSettings").on("resize.sectionSettings", builder_full_width_section);
function builder_full_width_section() {
    var $sections = jQuery('.main-page-wrapper .stretch_section');
    jQuery.each($sections, function (key, item) {
        var _sec = jQuery(this);
        var _sec_full = _sec.next(".section-current-width");
        _sec_full.length || (_sec_full = _sec.parent().next(".section-current-width"));
        var _sec_margin_left = parseInt(_sec.css("margin-left"), 10);
        var _sec_margin_right = parseInt(_sec.css("margin-right"), 10);
        var offset = 0 - _sec_full.offset().left - _sec_margin_left;
        var width = jQuery(window).width();
        if (_sec.css({
            position: "relative",
            left: offset,
            "box-sizing": "border-box",
            width: jQuery(window).width()
        }), !_sec.hasClass("stretch_data")) {
            var padding = -1 * offset;
            0 > padding && (padding = 0);
            var paddingRight = width - padding - _sec_full.width() + _sec_margin_left + _sec_margin_right;
            0 > paddingRight && (paddingRight = 0), _sec.css({
                "padding-left": padding + "px",
                "padding-right": paddingRight + "px"
            })
        }
    });
}

// Serialize Function
$.fn.serializeObject = function () {
    "use strict";
    var a = {}, b = function (b, c) {
        var d = a[c.name];
        "undefined" != typeof d && d !== null ? $.isArray(d) ? d.push(c.value) : a[c.name] = [d, c.value] : a[c.name] = c.value
    };
    return $.each(this.serializeArray(), b), a
};

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;
    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {
        }
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write
        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);
            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
                // To prevent the for loop in the first place assign an empty array
                // in case there are no cookies at all. Also prevents odd result when
                // calling $.cookie().
                cookies = document.cookie ? document.cookie.split('; ') : [],
                i = 0,
                l = cookies.length;
        for (; i < l; i++) {
            var parts = cookies[i].split('='),
                    name = decode(parts.shift()),
                    cookie = parts.join('=');
            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };
    config.defaults = {};
    $.removeCookie = function (key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {expires: -1}));
        return !$.cookie(key);
    };
}));

//Currency Positions
function currency_pos(currency,val) {
	var currency_p  		= scripts_vars.currency_pos;
	if( currency_p === 'right' ){
		return val+currency;
	}else if( currency_p === 'right_space' ){
		return val+' '+currency;
	}else if( currency_p === 'left_space' ){
		return currency+' '+val;
	} else{
		return currency+val;
	}
}
//Check Numeric Value only
!function(e){e.fn.numeric=function(t,i){"boolean"==typeof t&&(t={decimal:t}),void 0===(t=t||{}).negative&&(t.negative=!0);var n,a,r=!1===t.decimal?"":t.decimal||".",c=!0===t.negative;return i="function"==typeof i?i:function(){},"number"==typeof t.scale?0==t.scale?(r=!1,n=-1):n=t.scale:n=-1,a="number"==typeof t.precision?t.precision:0,this.data("numeric.decimal",r).data("numeric.negative",c).data("numeric.callback",i).data("numeric.scale",n).data("numeric.precision",a).keypress(e.fn.numeric.keypress).keyup(e.fn.numeric.keyup).blur(e.fn.numeric.blur)},e.fn.numeric.keypress=function(t){var i=e.data(this,"numeric.decimal"),n=e.data(this,"numeric.negative"),a=t.charCode?t.charCode:t.keyCode?t.keyCode:0;if(13==a&&"input"==this.nodeName.toLowerCase())return!0;if(13==a)return!1;var r=!1;if(t.ctrlKey&&97==a||t.ctrlKey&&65==a)return!0;if(t.ctrlKey&&120==a||t.ctrlKey&&88==a)return!0;if(t.ctrlKey&&99==a||t.ctrlKey&&67==a)return!0;if(t.ctrlKey&&122==a||t.ctrlKey&&90==a)return!0;if(t.ctrlKey&&118==a||t.ctrlKey&&86==a||t.shiftKey&&45==a)return!0;if(a<48||a>57){var c=e(this).val();if(0!==c.indexOf("-")&&n&&45==a&&(0===c.length||0===parseInt(e.fn.getSelectionStart(this),10)))return!0;i&&a==i.charCodeAt(0)&&-1!=c.indexOf(i)&&(r=!1),8!=a&&9!=a&&13!=a&&35!=a&&36!=a&&37!=a&&39!=a&&46!=a?r=!1:void 0!==t.charCode&&(t.keyCode==t.which&&0!==t.which?(r=!0,46==t.which&&(r=!1)):0!==t.keyCode&&0===t.charCode&&0===t.which&&(r=!0)),i&&a==i.charCodeAt(0)&&(r=-1==c.indexOf(i))}else if(e.data(this,"numeric.scale")>=0){var s=this.value.indexOf(i);s>=0?(decimalsQuantity=this.value.length-s-1,e.fn.getSelectionStart(this)>s?r=decimalsQuantity<e.data(this,"numeric.scale"):(integersQuantity=this.value.length-1-decimalsQuantity,r=integersQuantity<e.data(this,"numeric.precision")-e.data(this,"numeric.scale"))):r=!(e.data(this,"numeric.precision")>0)||this.value.replace(e.data(this,"numeric.decimal"),"").length<e.data(this,"numeric.precision")-e.data(this,"numeric.scale")}else r=!(e.data(this,"numeric.precision")>0)||this.value.replace(e.data(this,"numeric.decimal"),"").length<e.data(this,"numeric.precision");return r},e.fn.numeric.keyup=function(t){var i=e(this).val();if(i&&i.length>0){var n=e.fn.getSelectionStart(this),a=e.data(this,"numeric.decimal"),r=e.data(this,"numeric.negative");if(""!==a&&null!==a){var c=i.indexOf(a);0===c&&(this.value="0"+i),1==c&&"-"==i.charAt(0)&&(this.value="-0"+i.substring(1)),i=this.value}for(var s=[0,1,2,3,4,5,6,7,8,9,"-",a],u=i.length,l=u-1;l>=0;l--){var h=i.charAt(l);0!==l&&"-"==h?i=i.substring(0,l)+i.substring(l+1):0!==l||r||"-"!=h||(i=i.substring(1));for(var d=!1,o=0;o<s.length;o++)if(h==s[o]){d=!0;break}d&&" "!=h||(i=i.substring(0,l)+i.substring(l+1))}var m=i.indexOf(a);if(m>0){for(var f=u-1;f>m;f--){i.charAt(f)==a&&(i=i.substring(0,f)+i.substring(f+1))}e.data(this,"numeric.scale")>=0&&(i=i.substring(0,m+e.data(this,"numeric.scale")+1)),e.data(this,"numeric.precision")>0&&(i=i.substring(0,e.data(this,"numeric.precision")+1))}else e.data(this,"numeric.precision")>0&&(i=i.substring(0,e.data(this,"numeric.precision")-e.data(this,"numeric.scale")));this.value=i,e.fn.setSelection(this,n)}},e.fn.numeric.blur=function(){var t=e.data(this,"numeric.decimal"),i=e.data(this,"numeric.callback"),n=this.value;""!==n&&(new RegExp("^\\d+$|^\\d*"+t+"\\d+$").exec(n)||i.apply(this))},e.fn.removeNumeric=function(){return this.data("numeric.decimal",null).data("numeric.negative",null).data("numeric.callback",null).unbind("keypress",e.fn.numeric.keypress).unbind("blur",e.fn.numeric.blur)},e.fn.getSelectionStart=function(e){if(e.createTextRange){var t=document.selection.createRange().duplicate();return t.moveEnd("character",e.value.length),""===t.text?e.value.length:e.value.lastIndexOf(t.text)}return e.selectionStart},e.fn.setSelection=function(e,t){if("number"==typeof t&&(t=[t,t]),t&&t.constructor==Array&&2==t.length)if(e.createTextRange){var i=e.createTextRange();i.collapse(!0),i.moveStart("character",t[0]),i.moveEnd("character",t[1]),i.select()}else e.setSelectionRange&&(e.focus(),e.setSelectionRange(t[0],t[1]))}}(jQuery);