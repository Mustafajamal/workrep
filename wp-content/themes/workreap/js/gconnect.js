var googleUser = {};
var loader_html = '<div class="wt-preloader-section"><div class="wt-preloader-holder"><div class="wt-loader"></div></div></div>';
var workreap_gconnect_app = function() {
	gapi.load('auth2', function(){
	  auth2 = gapi.auth2.init({
		client_id: scripts_vars.gclient_id,
		cookiepolicy: 'single_host_origin',
	  });
		
	  attachSignin(document.getElementById('wt-gconnect'));
	  attachSignin(document.getElementById('wt-gconnect-reg'));
	  
	});
  };

  function attachSignin(element) {
	auth2.attachClickHandler(element, {},
		function(googleUser) {
			jQuery('body').append(loader_html);
			var profile = googleUser.getBasicProfile();

			var dataString = 'login_type=google&'+'picture=' + profile.getImageUrl()+'&email=' + profile.getEmail() +'&id=' + profile.getId() + '&name=' + profile.getName() + '&action=workreap_js_social_login';

			jQuery.ajax({
				type: "POST",
				url: scripts_vars.ajaxurl,
				data: dataString,
				dataType: "json",
				success: function (response) {
					jQuery('body').find('.wt-preloader-section').remove();
					if (response.type === 'success') {  
						jQuery('#loginpopup').modal('hide');
						
						if( typeof(response.html) != "undefined" && response.html !== null && response.html !== '') {
							jQuery('.modal-post-wrap').html(response.html);
							jQuery('.wt-registration-content-model').html(response.html);
							jQuery('.wt-registration-parent-model').modal('show');
							jQuery('#taskpopup').modal('show');
							
							var is_rtl  					= scripts_vars.is_rtl;
							
							var config = {
									'.chosen-select'           : {rtl:is_rtl},
									'.chosen-select-deselect'  : {allow_single_deselect:true},
									'.chosen-select-no-single' : {disable_search_threshold:10},
									'.chosen-select-no-results': {no_results_text:scripts_vars.nothing},
									'.chosen-select-width'     : {width:"95%"}
							}

							for (var selector in config) {
								jQuery(selector).chosen(config[selector]);
							}
						} else {
							jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000 });
							window.location.reload();
						}
					} else {
						jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
					}
				}
			});

		}, function(error) {
			//jQuery.sticky(JSON.stringify(error, undefined, 2), {classList: 'important', speed: 200, autoclose: 5000});
		});
  }
