<?php
/**
 *
 * The template used for displaying freelancer post style
 *
 * @package   Workreap
 * @author    amentotech
 * @link      https://amentotech.com/user/amentotech/portfolio
 * @version 1.0
 * @since 1.0
 */

get_header();
while ( have_posts() ) {
	the_post();
	global $post;
	$post_id = $post->ID;
	
	$linked_profile = get_post_meta($post_id, '_linked_profile', true);
	
	$freelancer_gallery_option	= '';
	if( function_exists('fw_get_db_settings_option')  ){
		$freelancer_gallery_option	= fw_get_db_settings_option('freelancer_gallery_option', $default_value = null);
	}

	$freelancer_specialization	= '';
	if( function_exists('fw_get_db_settings_option')  ){
		$freelancer_specialization	= fw_get_db_settings_option('freelancer_specialization', $default_value = null);
	}

	$freelancer_gallery_option	= '';
	if( function_exists('fw_get_db_settings_option')  ){
		$freelancer_gallery_option	= fw_get_db_settings_option('freelancer_gallery_option', $default_value = null);
	}
	
	$experience	= '';
	if( function_exists('fw_get_db_settings_option')  ){
		$experience	= fw_get_db_settings_option('freelancer_industrial_experience', $default_value = null);
	}
	
	?>
	<div class="wt-sectionspacetop wt-haslayout">
		<?php get_template_part('directory/front-end/templates/freelancer/single/banner'); ?>
		<div class="wt-sectionspacetop wt-haslayout">
			<?php 
				get_template_part('directory/front-end/templates/freelancer/single/basic'); 
				if( apply_filters('workreap_system_access','service_base') === true ){
					get_template_part('directory/front-end/templates/freelancer/single/services'); 
				}
			?>
			<div class="container">
				<div class="row">
					<div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-8 float-left">
							<div class="wt-usersingle">
								<?php 
									if(!empty($freelancer_gallery_option) && $freelancer_gallery_option === 'enable' ){
										get_template_part('directory/front-end/templates/freelancer/single/gallery');
									}
									if( apply_filters('workreap_system_access','job_base') === true ){
										get_template_part('directory/front-end/templates/freelancer/single/projects');
									}
									get_template_part('directory/front-end/templates/freelancer/single/crafted-videos'); 
									//get_template_part('directory/front-end/templates/freelancer/single/crafted-projects'); 
									//get_template_part('directory/front-end/templates/freelancer/single/experience'); 
									//get_template_part('directory/front-end/templates/freelancer/single/education');
								?>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-4 float-left">
							<aside id="wt-sidebar" class="wt-sidebar">
							<?php
								if(!empty($freelancer_specialization) && $freelancer_specialization === 'enable' ){
										get_template_part('directory/front-end/templates/freelancer/single/sidebar-specialization');
									}
								$english_level = get_post_meta($post_id, '_english_level', true);
								//echo '<div><ul class="styles"><li>'.$english_level.'</li></ul></div>';
								?>
							<div id="wt-ourskill" class="wt-widget items-more-wrap-sk toolip-wrapo">
								
								<div style="margin-top: 15px;" class="wt-widgettitle">
									<h2>Laguages</h2>
								</div>
								<?php
								$languages = get_the_term_list( $post_id, 'languages', '<ul class="styles"><li>', '</li><li>', '</li></ul>' );
								echo '<div>'.$languages.'</div>'; 
								?>		
							</div>
								<?php 
								//echo 'adasda';
								//	get_template_part('directory/front-end/templates/freelancer/single/sidebar-social'); 
									//get_template_part('directory/front-end/templates/freelancer/single/sidebar-resume'); 
									//get_template_part('directory/front-end/templates/freelancer/single/sidebar-skills'); 
									
									//if(!empty($experience) && $experience === 'enable' ){
										//get_template_part('directory/front-end/templates/freelancer/single/sidebar-industrial_experiences');
								//	}
									//get_template_part('directory/front-end/templates/freelancer/single/sidebar-awards');
									//do_action('workreap_get_qr_code','freelancer',intval( $post_id ));
									//get_template_part('directory/front-end/templates/freelancer/single/similar-freelancer');
									//get_template_part('directory/front-end/templates/freelancer/single/share'); 
									//do_action('workreap_report_post_type_form',$post_id,'freelancer');
								?>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php do_action('workreap_chat_modal',$linked_profile);?>
	<?php 
		$script = "
				jQuery(document).ready(function () {
					var read_more      	= scripts_vars.read_more;
					var less      		= scripts_vars.less;
					var _readmore = jQuery('.wt-userdetails .wt-description');
					_readmore.readmore({
						speed: 500,
						collapsedHeight: 247,
						moreLink: '<a class=\"wt-btntext\" href=\"javascript:;\">".esc_html__('Read More','workreap')."</a>',
						lessLink: '<a class=\"wt-btntext\" href=\"javascript:;\">".esc_html__('Less','workreap')."</a>',
					});
				});
			";
			wp_add_inline_script( 'readmore', $script, 'after' );
		} 

		get_footer(); 
