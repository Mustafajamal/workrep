<?php
/**
 *
 * Class used as base to create theme footer
 *
 * @package   Workreap
 * @author    amentotech
 * @link      https://themeforest.net/user/amentotech/portfolio
 * @since 1.0
 */
if (!class_exists('Workreap_Prepare_Footers')) {

    class Workreap_Prepare_Footers {

        function __construct() {
            add_action('workreap_do_process_footers', array(&$this, 'workreap_do_process_footers'));
        }

        /**
         * @Prepare Footer
         * @return {}
         * @author amentotech
         */
        public function workreap_do_process_footers() {?>
            </main>    
            <?php 
            //Reset Model
            if (!empty($_GET['key']) &&
                    ( isset($_GET['action']) && $_GET['action'] == "reset_pwd" ) &&
                    (!empty($_GET['login']) )
            ) {
                do_action('workreap_reset_password_form');
            }
			
			//hide for dashboard
			if (is_page_template('directory/dashboard.php')) {
				echo '</div>';
			} else{
				$this->workreap_do_process_footer_v1();
			}
			
			
        }
        
        /**
         * @Prepare Footer V1
         * @return {}
         * @author amentotech
         */
        public static function workreap_do_process_footer_v1() {
            $footer_type = array();
            $footer_copyright = 'Copyright &copy; ' . date('Y') . '&nbsp;' . esc_html__('Workreap. All rights reserved.', 'workreap') . get_bloginfo();
            $enable_footer_menu = '';
			
            if (function_exists('fw_get_db_settings_option')) {
                $footer_type = fw_get_db_settings_option('footer_type');
            }
			$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
			
            $menu  				= !empty($footer_type['footer_v1']['menu']) ? $footer_type['footer_v1']['menu'] : '';
            $footer_copyright   = !empty($footer_type['footer_v1']['copyright']) ? $footer_type['footer_v1']['copyright'] : $footer_copyright;
			$join   			= !empty($footer_type['footer_v1']['join']) ? $footer_type['footer_v1']['join'] : array();
			$widget_area   		= !empty($footer_type['footer_v1']['widget_area']) ? $footer_type['footer_v1']['widget_area'] : '';
			$footer_logo   		= !empty($footer_type['footer_v1']['footer_logo']) ? $footer_type['footer_v1']['footer_logo'] : '';
			$footer_content   	= !empty($footer_type['footer_v1']['footer_content']) ? $footer_type['footer_v1']['footer_content'] : '';
			$socials   			= !empty($footer_type['footer_v1']['socials']) ? $footer_type['footer_v1']['socials'] : array();
			$footer_links  				= !empty($footer_type['footer_v1']['footer_links']) ? $footer_type['footer_v1']['footer_links'] : '';

			$is_active_widgets	= 'wt-widgets-active';
			$login_footer	= '';
			
			if (!empty($join) && $join['gadget'] === 'enable' && is_user_logged_in() ) {
				$login_footer	= 'wt-joininfo';
			}
			
            ob_start();
            ?>
            <footer id="wt-footer" class="wt-footer wt-haslayout wt-footer-v1">
              <?php 
			  if( !empty( $footer_logo['url'] )
				  || !empty($footer_content)
				  || !empty($socials)
			  ){?>
				  <div class="wt-footerholder wt-haslayout wt-sectionspace <?php echo esc_attr( $is_active_widgets );?>">
					<div class="container">
					  <div class="row">
					    <?php 
						if( !empty( $footer_logo['url'] )
							  || !empty($footer_content)
							  || !empty($socials)
						){?>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							  <div class="wt-footerlogohold"> 
								<?php if( !empty( $footer_logo['url'] ) ){?>
									<strong class="wt-logo"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url( $footer_logo['url'] );?>" alt="<?php echo esc_attr($blogname); ?>"></a></strong>
								<?php }?>
								<?php if( !empty( $footer_content ) ){?>
									<div class="wt-description">
									  <?php echo do_shortcode( $footer_content );?>
									</div>
								<?php }?>
								<?php if( !empty( $socials ) ){?>
									<ul class="wt-socialiconssimple wt-socialiconfooter">
										<?php
											foreach ($socials as $key => $value) {
												$social_name = !empty($value['social_name']) ? $value['social_name'] : '';
												$social_class = !empty($value['social_icons_list']) ? $value['social_icons_list'] : '';
												$social_link = !empty($value['social_url']) ? $value['social_url'] : '#';
												$social_main_class = '';
												if (!empty($social_class)) {
													$social_main_class	= workreap_get_social_icon_name($social_class);
												}
												
												if (!empty($social_class)) {?>
													<li class="wt-<?php echo esc_attr($social_main_class); ?>"><a href="<?php echo esc_url($social_link); ?>"><i class="<?php echo esc_attr($social_class); ?>"></i></a></li>
													<?php
												}
											}
										?>
									</ul>
								<?php }?>
							  </div>
							</div>
						<?php }?>
						<?php 
						if( !empty( $footer_links ) ){
							$counter	= 0;
							foreach( $footer_links as $key => $value ){
								$counter++;
								?>
								<div class="col-12 col-sm-6 col-md-3 col-lg-3">
									<div class="wt-footercol wt-widgetcompany footer-link-<?php echo esc_attr( $counter );?>">
										<?php if( !empty( $value['heading'] ) ){?>
											<div class="wt-fwidgettitle">
												<h3><?php echo esc_html( $value['heading'] );?></h3>
											</div>
										<?php }?>
										<?php if( !empty( $value['links'] ) ){?>
											<ul class="wt-fwidgetcontent">
												<?php 
												foreach( $value['links'] as $lkey => $item ){
													if( !empty($item['text']) ){
												?>
													<li><a href="<?php echo esc_url( $item['link'] );?>"><?php echo esc_html( $item['text'] );?></a></li>
												<?php }}?>
												<?php if( !empty( $value['more'] ) ){?>
													<li class="wt-viewmore"><a href="<?php echo esc_url( $value['more'] );?>"><?php esc_html_e('+ View All','workreap');?></a></li>
												<?php }?>
											</ul>
										<?php }?>
									</div>
								</div>
						<?php }}?>
						<?php if ( is_active_sidebar('footer_sidebar_1') || is_active_sidebar('footer_sidebar_2') ) {?>
							<?php if (is_active_sidebar('footer_sidebar_1')) : ?>
								
							<?php endif; ?>
							<?php if (is_active_sidebar('footer_sidebar_2')) : ?>
								<div class="col-12 col-sm-6 col-md-3 col-lg-3">
									<div class="wt-footercol wt-widgetexplore"><?php dynamic_sidebar('footer_sidebar_2'); ?></div>
								</div>
							<?php endif; ?>
						<?php }?>
					  </div>
					</div>
				  </div>
			  <?php }?>
			  <?php if (!empty($join) && $join['gadget'] === 'enable' && !is_user_logged_in() ) { ?>
			  <div class="wt-haslayout wt-joininfo">
				<div class="container">
				  <div class="row justify-content-md-center">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 push-lg-1">
					  <?php 
						if( !empty( $join['enable']['title'] ) ){
							$content = strip_tags($join['enable']['title'], '<a><br>');
							?>
							<div class="wt-companyinfo"><span><?php echo do_shortcode($content); ?></span></div>
						<?php }?>
					  <?php if( !empty( $join['enable']['page_url'] ) ){?><div class="wt-fbtnarea"> <a href="<?php echo esc_url($join['enable']['page_url']); ?>" class="wt-btn"><?php esc_html_e('Join Now', 'workreap');?></a></div><?php }?>
					</div>
				  </div>
				</div>
			  </div>
			  <?php } ?>
			  <div class="wt-haslayout wt-footerbottom <?php echo esc_attr( $login_footer );?>">
				<div class="container">
				  <div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				  	  <?php if (!empty($footer_copyright)) { ?>
							<p class="wt-copyrights"><?php echo do_shortcode($footer_copyright); ?></p>
					  <?php } ?>
					  <?php if (isset($menu) && $menu === 'enable') { ?>
						  <nav class="wt-addnav">
							<?php Workreap_Prepare_Headers::workreap_prepare_navigation('footer-menu', '', '', '0'); ?>
						  </nav>
					  <?php } ?>
					</div>
				  </div>
				</div>
			  </div>
			</footer>
            </div>
            <?php
            echo ob_get_clean();
        }

    }

    new Workreap_Prepare_Footers();
}