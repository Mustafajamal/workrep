<?php
/**
 * @Set Post Views
 * @return {}
 */
if (!function_exists('workreap_add_dynamic_styles')) {

    function workreap_add_dynamic_styles() {
        if (function_exists('fw_get_db_settings_option')) {
            $color_base = fw_get_db_settings_option('color_settings');
			$logo_x = fw_get_db_settings_option('logo_x');
			$logo_y = fw_get_db_settings_option('logo_y');
			$body_bg_color = fw_get_db_settings_option('body_bg_color');
            $enable_typo = fw_get_db_settings_option('enable_typo');
            $background = fw_get_db_settings_option('background');
            $custom_css = fw_get_db_settings_option('custom_css');
            $body_font = fw_get_db_settings_option('body_font');
            $body_p = fw_get_db_settings_option('body_p');
            $h1_font = fw_get_db_settings_option('h1_font');
            $h2_font = fw_get_db_settings_option('h2_font');
            $h3_font = fw_get_db_settings_option('h3_font');
            $h4_font = fw_get_db_settings_option('h4_font');
            $h5_font = fw_get_db_settings_option('h5_font');
            $h6_font = fw_get_db_settings_option('h6_font');
			$freelancer_overlay = fw_get_db_settings_option('freelancer_overlay', true);
			$employer_overlay = fw_get_db_settings_option('employer_overlay', true);
			$footer_settings = fw_get_db_settings_option('footer_settings', true);
			
			
			$nrf_favorites 	= fw_get_db_settings_option('nrf_favorites', true);
			$nrf_messages 	= fw_get_db_settings_option('nrf_messages', true);
			$nrf_create 	= fw_get_db_settings_option('nrf_create', true);
			$nrf_found 		= fw_get_db_settings_option('nrf_found', true);
			$nrf_users 		= fw_get_db_settings_option('nrf_users', true);
        }
		
        $post_name = workreap_get_post_name();
		
		if( is_404() 
			|| is_archive() 
			|| is_search() 
			|| is_category() 
			|| is_tag() 
		) {

			if(function_exists('fw_get_db_settings_option')){
				$titlebar_type 	= fw_get_db_settings_option('titlebar_type', true);
				if(  isset( $titlebar_type['gadget'] ) 
					 && $titlebar_type['gadget'] === 'default' 
				) {
					$titlebar_overlay 		= !empty( $titlebar_type['default']['titlebar_overlay'] ) ? $titlebar_type['default']['titlebar_overlay'] : '';
				}
			}
		} else{
			$object_id = get_queried_object_id();
			if((get_option('show_on_front') && get_option('page_for_posts') && is_home()) ||
				(get_option('page_for_posts') && is_archive() && !is_post_type_archive()) && !(is_tax('product_cat') || is_tax('product_tag')) || (get_option('page_for_posts') && is_search())) {
					$page_id = get_option('page_for_posts');		
			}else {
				if(isset($object_id)) {
					$page_id = $object_id;
				}
			}
			
			if(function_exists('fw_get_db_settings_option')){
				$titlebar_type 		= fw_get_db_post_option($page_id, 'titlebar_type', true);
				if(  isset( $titlebar_type['gadget'] ) && ( $titlebar_type['gadget'] === 'custom' ) ){
					$titlebar_overlay 		= !empty( $titlebar_type['custom']['titlebar_overlay'] ) ? $titlebar_type['custom']['titlebar_overlay'] : '';
				} else {
					$titlebar_type 			= fw_get_db_settings_option('titlebar_type', true);
					$titlebar_overlay 		= !empty( $titlebar_type['default']['titlebar_overlay'] ) ? $titlebar_type['default']['titlebar_overlay'] : '';
				}

			}
		}

		
        ob_start();

        if (isset($enable_typo) && $enable_typo == 'on') { ?>
            body{<?php echo workreap_extract_typography($body_font); ?>}
            body p{<?php echo workreap_extract_typography($body_p); ?>}
            body ul {<?php echo workreap_extract_typography($body_font); ?>}
            body li {<?php echo workreap_extract_typography($body_font); ?>}
            body h1{<?php echo workreap_extract_typography($h1_font); ?>}
            body h2{<?php echo workreap_extract_typography($h2_font); ?>}
            body h3{<?php echo workreap_extract_typography($h3_font); ?>}
            body h4{<?php echo workreap_extract_typography($h4_font); ?>}
            body h5{<?php echo workreap_extract_typography($h5_font); ?>}
            body h6{<?php echo workreap_extract_typography($h6_font); ?>}
        <?php } ?>

        <?php
        if (isset($color_base['gadget']) && $color_base['gadget'] === 'custom') {
            if (!empty($color_base['custom']['primary_color'])) {
                $theme_color = $color_base['custom']['primary_color'];
				$footer_bg_color = !empty( $color_base['custom']['footer_bg_color'] ) ? $color_base['custom']['footer_bg_color'] : '';
				$footer_text_color = !empty( $color_base['custom']['footer_text_color'] ) ? $color_base['custom']['footer_text_color'] : '';
				
                $theme_color = apply_filters('workreap_get_page_color', $theme_color);
                ?>

                a,
				p a,
				p a:hover,
				a:hover,
				a:focus,
				a:active,
				.wt-navigation>ul>li:hover>a,
				.wt-navigation>ul>li.current-menu-item>a,
				.wt-navarticletab li:hover a,
				.wt-navarticletab li a.active,
				.wt-categoriescontent li a:hover,
				.wt-joinsteps li.wt-active a,
				.wt-effectivecontent li:hover a,
				.wt-filtertag li:hover a,
				.wt-userlisting-breadcrumb li .wt-clicksave,
				.wt-clicksave,
				.wt-qrcodefeat h3 span,
				.wt-comfollowers ul li:hover a span,
				.wt-postarticlemeta .wt-following span,
				.tg-qrcodefeat h3 span,
				.woocommerce-cart .woocommerce .product-name a:hover,
				.menu-pages-container .menu li:hover a,
				.widget_recent_entries>ul li a:hover,
				.widget_recent_comments>ul>li a:hover,
				.widget_archive>ul li a:hover,
				.widget_categories>ul>li a:hover,
				.widget_pages>ul li a:hover,
				.widget_nav_menu ul li .sub-menu li a:hover,
				.wp-block-archives li a:hover,
				.wp-block-latest-comments li a:hover,
				.wp-block-categories li a:hover,
				.wp-block-latest li a:hover,
				.wp-block-latest-posts li a:hover,
				.woocommerce ul.products li.product .price,
				.woocommerce div.product p.price,
				.woocommerce div.product span.price,
				.wt-ratingtitle h3,
				.wt-ratingcontent p em,
				.wt-videoshow a,
				.wt-clicksavebtn:hover,
				.wt-title h2 a:hover,
				.wt-particlecontent h3 a:hover,
				.blog-list-view-template .wt-title a:hover,
				.has-strong-theme-color-color cite,
				.has-text-color.has-strong-theme-color-color,
				.has-text-color.has-strong-theme-color-color cite,
				.woocommerce .product .entry-summary>.price,
				.wt-freelancers-content .dc-title span strong,
				.wcv-dashboard-navigation ul li:hover a,
				.wcvendors-pro-dashboard-wrapper .wcv-navigation ul.menu.black li a:hover,
				.wcvendors-pro-dashboard-wrapper .wcv-navigation ul.menu.black li.active>a,
				#wcv-store-settings .wcv-tabs.top>.tabs-nav li a:hover,
				#wcv-store-settings .wcv-tabs.top>.tabs-nav li.active a,
				#wcv-product-edit .wcv-tabs.top>.tabs-nav li a:hover,
				#wcv-product-edit .wcv-tabs.top>.tabs-nav li.active a,
				.wt-bannerholderthree .wt-bannercontent .wt-title h1 em
				{color:<?php echo esc_html($theme_color);?>;}
           
           		.wt-btn:hover,
				.wt-servicesgallery .wt-item-video figure,
				.infoBox .wt-mapcompanycontent .wt-viewjobholder ul li.wt-btnarea a,
				.wt-dropdowarrow,
				.navbar-toggle,
				.wt-btn,
				.wt-navigationarea .wt-navigation>ul>li>a:after,
				.wt-searchbtn,
				.wt-sectiontitle:after,
				.wt-navarticletab li a:after,
				.wt-pagination ul li a:hover,
				.wt-pagination ul li.wt-active a,
				.wt-widgettag a:hover,
				.tagcloud a:hover,
				.wt-articlesingle-content .wt-description blockquote span i,
				.wt-searchgbtn,
				.wt-filtertagclear a,
				.ui-slider-horizontal .ui-slider-range,
				.wt-btnsearch,
				.wt-newnoti a em,
				.wt-notificationicon>a:after,
				.wt-nav .navbar-toggler,
				.wt-usersidebaricon span,
				.wt-usersidebaricon span i,
				.wt-filtertag .wt-filtertagclear a,
				.wt-loader:before,
				.wt-offersmessages .wt-ad:after,
				.wt-btnsendmsg,
				.wt-tabstitle li a:before,
				.wt-tabscontenttitle:before,
				.wt-tablecategories thead tr th:first-child:before,
				.wt-tablecategories tbody tr td:first-child:before,
				.wt-slidernav .wt-prev:hover,
				.wt-slidernav .wt-next:hover,
				#confirmBox .button,
				.tg-qrcodedetails,
				.wt-userlistinghold .wt-widgettag a.showmore_skills,
				.woo-pagination span,
				.woo-pagination span:hover,
				.wt-featuredtagvtwo,
				.wt-reasonbtn,
				.woo-pagination a:hover,
				.wt-pagination ul li span.post-page-numbers,
				.woo-pagination span,
				#place_order,
				.woo-pagination span:hover,
				.woo-pagination a:hover,
				.woocommerce #respond input#submit,
				.wc-stripe-checkout-button,
				.woocommerce a.button,
				.woocommerce button.button,
				.woocommerce input.button,
				.woocommerce-MyAccount-content input[type=submit],
				.woocommerce-MyAccount-content .woocommerce-Button.button,
				.woocommerce ul.products li.product .button:hover,
				.woocommerce ul.products li.product .onsale,
				.woocommerce span.onsale,
				.woocommerce #respond input#submit.alt,
				.woocommerce a.button.alt,
				.woocommerce button.button.alt,
				.woocommerce input.button.alt,
				.woocommerce button.button.alt:hover,
				.woocommerce #review_form #respond .form-submit input:hover,
				.woo-pagination a:hover,
				.woo-pagination span.current,
				.woocommerce div.product .entry-summary form.cart .button,
				.has-strong-theme-color-background-color,
				.woocommerce a.button:hover,
				.woocommerce button.button:hover,
				.woocommerce input.button:hover,
				.woocommerce #respond input#submit.alt:hover,
				.woocommerce a.button.alt:hover,
				.woocommerce input.button.alt:hover,
				.widget_product_search button[type=submit],
				.fw-btn-1,
				.fw-btn-1:hover,
				.fw-tabs-container .fw-tabs ul li:before,
				.fw-package .fw-heading-row,
				.fw-package .fw-pricing-row,
				input[name="apply_for_vendor_submit"],
				.wcv-dashboard-navigation ul li a:after,
				table.table-vendor-sales-report thead tr th:first-child:before,
				table.table-vendor-sales-report tbody tr td:first-child:before,
				.wcv-dashboard-navigation~form p input[type="submit"].btn-small,
				#pv_shop_description_container~.btn-inverse,
				.wcvendors-pro-dashboard-wrapper .wcv-navigation ul.menu.black>li>a:after,
				.button.product,
				.wcvendors-pro-dashboard-wrapper .wcv-button.expand,
				table.wcvendors-table tbody tr td:first-child:before,
				#wcv-store-settings .wcv-tabs.top input[type="submit"],
				#wcv-store-settings .wcv-tabs.top>.tabs-nav li a:after,
				#wcv-product-edit .wcv-tabs.top input[type="submit"],
				#wcv-product-edit .wcv-tabs.top>.tabs-nav li a:after,
				.wcv-form .wcv_product_attributes .control-group .button,
				.wcv-form .wcv_product_attributes .control-group button,
				.wcv-form .wcv_product_attributes .control-group input[type="button"],
				#wcv_shipping_rates_table thead tr th:first-child:before,
				#wcv_shipping_rates_table tbody tr td:first-child:before,
				.wcv-search-form .control.append-button .wcv-button,
				.wcv-opening-hours-wrapper table thead tr th:first-child:before,
				.wcv-opening-hours-wrapper table tbody tr td:first-child:before,
				.wcvendors-table-product thead tr th:first-child:before,
				.download_file_table thead tr th:first-child:before,
				.download_file_table tbody tr td:first-child:before,
				.woocommerce div.product .woocommerce-tabs ul.tabs li a:after,
				#wcv-shop_coupon-edit.wcv-form input[type="submit"],
				#wcv-shop_coupon-edit.wcv-form .wcv-tabs .tabs-nav li a:after,
				.wcv-grid .wcv_recent_orders .wcv-button,
				.resume-item-download,
				.form-group-label label span, 
				.form-group-label label span:hover, 
				.wt-categoriesholder .wt-tablecategories tbody tr td .wt-actionbtn a.wt-deleteinfo, 
				.wt-msgbtn
				{ background:<?php echo esc_html($theme_color);?>; }
          		
          		.wt-sticky .wt-headervthhree .wt-navigationarea .wt-navigation > ul > li > a:after,
          		.wc-forward{ background-color:<?php echo esc_html($theme_color);?> !important; }
           
           		input:focus,
				.select select:focus,
				.form-control:focus,
				.wt-navigation>ul>li>.sub-menu,
				.wt-pagination ul li a:hover,
				.tagcloud a:hover,
				.wt-widgettag a:hover,
				.wt-joinsteps li.wt-active a,
				.wt-filtertag li:hover a,
				.wt-filtertag .wt-filtertagclear a,
				.wt-themerangeslider .ui-slider-handle,
				.wt-clicksavebtn,
				.wt-pagination ul li.wt-active a,
				.wt-usernav>ul,
				.wt-userlistinghold .wt-widgettag a.showmore_skills,
				.woo-pagination span,
				.woo-pagination span:hover,
				.woo-pagination a:hover,
				.wt-navigation>ul>li .children,
				.wt-pagination ul li span.post-page-numbers,
				.woo-pagination a:hover,
				.woo-pagination span.current,
				blockquote:not(.blockquote-link),
				.wp-block-quote.is-style-large,
				.wp-block-quote:not(.is-large):not(.is-style-large),
				.wp-block-quote.is-style-large,
				.fw-btn-1
           		{ border-color: <?php echo esc_html($theme_color);?>; }
           		
           		.wt-filtertag .wt-filtertagclear a:after{ border-left-color: <?php echo esc_html($theme_color);?>; }

				/* RGBA Background Color */
				.wt-loader{
					background: <?php echo esc_html($theme_color);?>;
					background: -moz-linear-gradient(left, <?php echo esc_html($theme_color);?> 10%, rgba(255, 88, 81, 0) 42%);
					background: -webkit-linear-gradient(left, <?php echo esc_html($theme_color);?> 10%, rgba(255, 88, 81, 0) 42%);
					background: -o-linear-gradient(left, <?php echo esc_html($theme_color);?> 10%, rgba(255, 88, 81, 0) 42%);
					background: -ms-linear-gradient(left, <?php echo esc_html($theme_color);?> 10%, rgba(255, 88, 81, 0) 42%);
					background: linear-gradient(to right, <?php echo esc_html($theme_color);?> 10%, rgba(255, 88, 81, 0) 42%);
				}
				.post-password-form p input[type=submit]{background: <?php echo esc_html($theme_color);?> !important;}
           
            <?php } ?>
            <?php
        }
		
		if( !empty( $logo_x ) && !empty( $logo_y ) ){?>
			.wt-logo{max-width:none;}
			.wt-logo a img{width:<?php echo esc_html( $logo_x );?>px; height:<?php echo esc_html( $logo_y );?>px;}
		<?php } else if( !empty( $logo_x ) ){?>
			.wt-logo{max-width:none;}
			.wt-logo a img{width:<?php echo esc_html( $logo_x );?>px;}
		<?php }
		if( !empty( $body_bg_color ) ){?>
			main.wt-main{background: <?php echo esc_html($body_bg_color);?> !important;}
		<?php
		}
		
		if( !empty( $freelancer_overlay ) ){?>
		.single-micro-services .wt-companysimg,
		.single-freelancers .frinnerbannerholder:after{background:<?php echo esc_html( $freelancer_overlay );?>;}
		<?php }
		
		if( !empty( $employer_overlay ) ){?>
		.page-template-employer-search .wt-companysimg,
		.single-projects .wt-companysimg,
		.single-employers .wt-comsingleimg figure{background:<?php echo esc_html( $employer_overlay );?>;}
        <?php }
		
		if( !empty( $nrf_found['url'] ) ){?>
			.wt-emptydetails span{background:url(<?php echo esc_url( $nrf_found['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $nrf_found['url'] ) ){?>				  
			.wt-empty-invoice span{background:url(<?php echo esc_url( $nrf_found['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $nrf_user['url'] ) ){?>	
			.wt-empty-person span{background:url(<?php echo esc_url( $nrf_user['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $nrf_create['url'] ) ){?>	
			.wt-empty-projects span{background:url(<?php echo esc_url( $nrf_create['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $nrf_messages['url'] ) ){?>	
			.wt-empty-message span{background:url(<?php echo esc_url( $nrf_messages['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $nrf_favorites['url'] ) ){?>	
		.wt-empty-saved span{background:url(<?php echo esc_url( $nrf_favorites['url'] );?>);margin-bottom: 30px;}
		<?php }
		
		if( !empty( $titlebar_overlay ) ){?>
		.wt-titlebardynmic.wt-innerbannerholder:before{background-color:<?php echo esc_html( $titlebar_overlay );?>;}
        <?php }
		 if (isset($footer_settings['gadget']) && $footer_settings['gadget'] === 'custom' && $footer_settings['custom']['footer_bg_color'] && !empty( $footer_settings['custom']['footer_text_color'] )) {?>
			.wt-footer{background:<?php echo esc_html( $footer_settings['custom']['footer_bg_color'] );?>;}
			<?php if( !empty( $footer_settings['custom']['footer_text_color'] ) ){?>
				.wt-footer .wt-socialiconfooter.wt-socialiconssimple li a i,
				.wt-footer .wt-footerholder .wt-fwidgetcontent li a,
				.wt-footer .wt-footerholder .wt-fwidgettitle h3,
				.wt-footer .wt-footerholder .wt-fwidgetcontent li.wt-viewmore a,
				.wt-footer .wt-copyrights,
				.wt-footer .wt-addnav ul li a,
				.wt-footer .wt-footerlogohold .wt-description p a,
				.wt-footer .wt-footerlogohold>.wt-description>p{color:<?php echo esc_html( $footer_settings['custom']['footer_text_color'] );?>;}
			<?php }?>
		<?php }
		echo (isset($custom_css)) ? $custom_css : '';
		
        return ob_get_clean();
    }
}