<?php

/**
 * @package   Workreap Core
 * @author    Amentotech
 * @link      http://amentotech.com/
 * @version 1.0
 * @since 1.0
 */
if (!class_exists('Workreap_Dispute')) {

    class Workreap_Dispute {

        /**
         * @access  public
         * @Init Hooks in Constructor
         */
        public function __construct() {
            add_action('init', array(&$this, 'init_directory_type'));	
        }

        /**
         * @Init Post Type
         * @return {post}
         */
        public function init_directory_type() {
            $this->prepare_post_type();
        }

        /**
         * @Prepare Post Type Category
         * @return post type
         */
        public function prepare_post_type() {
            $labels = array(
                'name'				=> esc_html__('Disputes', 'workreap_core'),
                'all_items' 		=> esc_html__('Disputes', 'workreap_core'),
                'singular_name' 	=> esc_html__('Disputes', 'workreap_core'),
                'add_new' 			=> esc_html__('Add Dispute', 'workreap_core'),
                'add_new_item' 		=> esc_html__('Add New Dispute', 'workreap_core'),
                'edit' 				=> esc_html__('Edit', 'workreap_core'),
                'edit_item' 		=> esc_html__('Edit Dispute', 'workreap_core'),
                'new_item' 			=> esc_html__('New Dispute', 'workreap_core'),
                'view' 				=> esc_html__('View Dispute', 'workreap_core'),
                'view_item' 		=> esc_html__('View Dispute', 'workreap_core'),
                'search_items' 		=> esc_html__('Search Dispute', 'workreap_core'),
                'not_found' 		=> esc_html__('No Dispute found', 'workreap_core'),
                'not_found_in_trash'=> esc_html__('No Dispute found in trash', 'workreap_core'),
                'parent' 			=> esc_html__('Parent Dispute', 'workreap_core'),
            );
            $args = array(
                'labels' 				=> $labels,
                'description' 			=> esc_html__('This is where you can add new Dispute ', 'workreap_core'),
                'public' 				=> true,
                'supports' 				=> array('title','editor'),
                'show_ui' 				=> true,
                'capability_type' 		=> 'post',
                'map_meta_cap' 			=> true,
                'publicly_queryable' 	=> false,
                'exclude_from_search' 	=> false,
                'hierarchical' 			=> false,
				'show_in_menu' 			=> 'edit.php?post_type=freelancers',
                'menu_position' 		=> 10,
                'rewrite' 				=> array('slug' => 'dispute', 'with_front' => true),
                'query_var' 			=> false,
                'has_archive' 			=> false,
				'capabilities' 			=> array('create_posts' => false)
            );
            register_post_type('disputes', $args);     
        }
    }

    new Workreap_Dispute();
}


