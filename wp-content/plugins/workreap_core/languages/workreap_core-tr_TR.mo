��    �     �      |+      |+  '   }+  	   �+     �+     �+  2   �+  
   ,     ,     ,     ,     ,  	   ,     ),     +,     D,     \,     |,     �,     �,     �,     �,  !   �,     �,     -     $-     B-     a-     q-  
   �-     �-  +   �-  
   �-  
   �-     �-     �-     .     .     .     %.     4.  3   Q.     �.  *   �.     �.     �.     �.     �.      /     /     #/     4/     G/     X/     i/     /     �/     �/     �/     �/     �/     �/     0  L   0     ]0  W   q0     �0     �0  
   �0  
   �0     �0  	   
1     1  (   1  *   E1     p1  (   �1     �1  0   �1  )   �1  /   2  4   E2  (   z2     �2  *   �2  -   �2  +   3     83  3   A3  -   u3     �3  O   �3     4  0   4  ,   @4  0   m4  .   �4  *   �4     �4  	   5     5  &   5  %   D5     j5     z5  4   �5  	   �5  "   �5  S   �5  )   A6      k6     �6  '   �6  (   �6  (   �6  &   7  &   =7     d7  )   u7     �7     �7  
   �7     �7     �7     �7     8     8     8     /8  	   C8     M8  
   f8     q8  	   x8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     9     9     9     '9     49     @9     M9     [9     c9  
   9     �9     �9     �9     �9     �9     �9     �9     �9     
:     :     &:     ?:  5   V:     �:  $   �:     �:     �:     �:  $   ;  (   3;     \;     d;  	   m;     w;     �;     �;     �;     �;     �;     �;  
   �;     �;     �;     �;     �;     �;     	<     <  
   <     "<     0<     C<     S<     `<     n<     ~<     �<     �<     �<     �<     �<     �<     �<     �<     =  
   =     &=     ,=     D=     _=  ,   x=     �=  	   �=     �=  	   �=  
   �=     �=     �=     >     >     #>     ,>     :>     @>     I>  
   R>     ]>  	   t>     ~>     �>  	   �>     �>  
   �>     �>     �>     �>     �>     �>     	?     ?     *?     1?     @?     X?     o?     {?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     @     @     "@     2@     ?@     \@     m@     y@  	   �@     �@     �@  _   �@     A     A      A     .A  
   <A     GA     LA     XA     kA     }A     �A     �A     �A     �A     �A     �A  	   �A  	   �A     B     B     +B     0B     <B     KB     TB     bB     kB     |B     �B     �B     �B     �B     �B  	   �B  	   �B     �B  	   �B  4   C     7C     <C     BC     GC     UC     bC     sC     �C     �C     �C     �C     �C     �C     �C     �C     �C     D     #D     7D     CD     PD     _D     qD     �D     �D     �D     �D     �D  
   �D     �D  
   E     E     *E     <E     KE     SE     XE     `E     cE     tE     �E     �E     �E     �E     �E     F     F     )F     @F     `F  #   {F     �F     �F     �F     �F     �F     G     G     6G     FG     _G     vG     �G     �G     �G     �G  2   �G     H     /H     CH     UH     oH     �H     �H     �H     �H     �H     �H     I     I     I     (I     6I     RI     cI     vI     �I  "   �I     �I     �I     �I     �I     J     J     #J     8J     NJ     `J     sJ     �J     �J     �J     �J     �J     �J     �J     �J     K     $K     5K     DK     YK     oK     ~K     �K     �K     �K     �K     �K     �K  #   �K     L  	   5L     ?L     FL     UL     \L     xL     �L  	   �L  
   �L  !   �L  E   �L  D   M     JM     [M  
   tM  
   M     �M     �M     �M     �M     �M     �M     �M     �M     �M     �M     N     )N  	   9N     CN     ON     \N     cN     kN     sN  *   �N     �N     �N     �N     �N     �N     �N     �N     	O     O  	   !O     +O  	   7O  	   AO  	   KO     UO     ]O     iO     oO     |O     �O     �O     �O     �O     �O     �O     �O     �O     P     P     ,P  
   AP     LP     dP     sP     �P     �P     �P     �P     �P     �P     �P     �P     Q  #   Q     CQ  $   aQ     �Q     �Q     �Q      �Q     �Q     �Q     
R     $R     0R     8R  
   MR     XR     `R     iR     {R     �R     �R     �R     �R     �R     �R     �R     �R     S     S     )S     :S     NS     ZS     bS     kS     rS     xS     S     �S     �S  )   �S  (   �S  )   �S     T     %T  >   <T     {T  	   �T     �T  
   �T     �T     �T     �T     �T     �T  	   �T     �T  	   �T     �T     U     U     %U     +U     >U     SU  
   bU  	   mU  %   wU     �U     �U  (   �U  &   �U  +   %V  &   QV  +   xV  %   �V  (   �V  '   �V  &   W  &   BW  2   iW  D   �W     �W     �W     �W     �W     X     X     X     'X     .X     @X     HX     UX     gX     tX     �X     �X     �X     �X     �X     �X     �X     Y  %   Y     8Y     MY     ZY  %   hY     �Y  *   �Y  /   �Y     �Y  ]    Z     ^Z  	   yZ  ,   �Z     �Z     �Z     �Z     �Z  	   [  (   #[  "   L[  
   o[     z[  	   �[  
   �[     �[  	   �[     �[     �[     �[     �[     �[     \     \     #\     1\     =\     I\     \\     n\     v\     �\     �\     �\     �\     �\     �\     �\     �\     �\     ]  �  %]  %   �^  !   �^     _  2   _  -   P_     ~_  )   �_     �_  �   �_  X   [`     �`     �`  
   �`     �`     �`  '   �`  1   a     La     Oa     Ta     ma     �a     �a     �a     �a     �a     �a  �  �a  .   �c     �c     �c     �c  ;   �c     %d     -d     1d     4d     7d     :d     Bd     Dd     _d     ud     �d     �d     �d     �d     �d  ,   �d  $   e  *   &e  (   Qe  (   ze     �e     �e  
   �e  "   �e  G   �e  
   =f     Hf     Uf     af     qf     �f     �f     �f  +   �f  K   �f  	   'g  ?   1g     qg     �g     �g     �g     �g     �g     �g     h     #h     2h     Ch     ^h     {h     �h     �h     �h     �h     �h     �h  _   i     li     �i     j     j     j     &j     2j     Ij     Wj  <   `j  =   �j     �j  ;   �j     2k  B   Kk  9   �k  8   �k  <   l  .   >l  
   ml  7   xl  B   �l  A   �l  
   5m  =   @m  ;   ~m     �m  h   �m  	   8n  >   Bn  9   �n  =   �n  @   �n  9   :o     to  
   �o     �o  1   �o  -   �o     �o     p  F   %p     lp  -   zp  q   �p  5   q  (   Pq     yq  0   �q  7   �q  6   �q  5   *r  4   `r     �r  >   �r     �r  ,   �r      s     /s     As     Xs  	   fs     ps     s     �s     �s     �s  
   �s     �s     �s     �s  !   t     8t     Dt     Kt     \t     jt  	   pt     zt     �t     �t     �t     �t     �t     �t     �t     �t     �t     u     $u      2u     Su     `u     vu  	   �u     �u     �u     �u     �u     �u     �u     v     v  (   8v  =   av     �v  !   �v     �v  
   �v     �v  #   w  "   Aw     dw  	   lw     vw     �w     �w     �w     �w     �w     �w     �w     �w  
   �w     �w     �w     x  
   x     x  	    x     *x     ;x     Px     kx     x     �x     �x     �x     �x     �x     �x     y     (y     :y     Ky     gy     xy     �y     �y     �y     �y     �y  .   �y  
   ,z  
   7z     Bz     Yz     gz     xz     �z  
   �z     �z  	   �z     �z     �z     �z     �z     {     {     '{     5{     F{     W{     c{     p{     �{     �{     �{     �{  
   �{     �{     �{     �{     �{     |     |     ?|     K|     e|     n|     s|     �|     �|     �|  	   �|     �|     �|     �|     �|     }     }     }     1}  !   A}     c}     y}     �}  	   �}     �}     �}  �   �}     _~  	   h~     r~     �~     �~  	   �~     �~     �~     �~     �~       -   4     b     k     y     �     �     �     �     �  	   �     �     �     �     �  	   �     �     -�     3�  (   C�     l�     y�     �     ��  	   ��     ��     ��  C   À     �     �     �     �     *�     9�     I�     c�     k�     |�     ��     ��     ��     ҁ     ׁ     �     ��     �     +�     ?�     N�     _�     n�     �     ��     ��     ��     ��     ނ     �     �     �     0�     G�  
   Y�     d�     j�     v�     ~�  !   ��     ��     ԃ  &   �     �  '   *�     R�     e�     w�  )   ��     ��  +   ׄ     �     �     .�     I�  #   ]�     ��  "   ��     ��     ʅ     �  -   �     1�     L�     _�     r�  ;   ��          �     ��  %   �  &   2�     Y�     j�     ��  !   ��     ��     ʇ     �  	   ��     �     �     .�  !   M�     o�     ��     ��  *        �     �     �     0�     ?�     P�     b�     z�     ��     ��     ��     ǉ     ։     �  	   �     ��     	�     �     (�     <�     J�     Y�     g�     ~�     ��     ��     ��     ϊ     ݊     �     �      �  %   �     ?�     [�     d�     l�     {�     ��  
   ��     ��  	   ��     ��  -   ŋ  A   �  >   5�     t�     ��     ��     ��     Č     ��     �     ��     ��     �     �  	   +�     5�     =�     V�     g�     w�     �     ��     ��     ��     ��     Ӎ  A   ��     "�     )�     7�     O�  	   i�     s�     ��     ��     ��     ю     �     ��     �     "�  	   3�     =�     Q�     W�  
   p�     {�     ��     ��     ��     ˏ     ޏ     ��     �     �     $�     0�     <�     M�     g�     v�     ��     ��     ��     ��     Ԑ     �     ��  !   �     /�  7   ?�  &   w�  /   ��     Α  	   �     ��  /    �     0�     O�  $   _�  
   ��  
   ��     ��     ��     ��     ɒ     В     �     ��     �     %�     :�  	   K�     U�     ^�     t�     ��     ��     ��          ޓ  
   ��     �     �     �     "�     *�     1�     >�  B   G�  B   ��  9   ͔     �     $�  V   D�     ��     ��     ��     ̕     ە     �     �     �     ��  	   �     �  	   (�     2�     H�     c�  	   j�     t�     ��     ��     ��     ��  5   Ζ  &   �     +�  9   G�  6   ��  1   ��  0   �  5   �  *   Q�  /   |�  -   ��  +   ژ  -   �  &   4�  H   [�     ��     ��  
   ��     ę     ؙ     �     �     ��      �     �     �     4�     L�     ^�     t�     ��     ��     ��     ˚     �     ��     �  5   *�     `�     y�     ��  5   ��     ܛ  *   �  @   �     [�  q   _�  !   ќ     �  8   �  $   >�     c�     ��  %   ��     Ý  (   ֝  &   ��     &�     ;�  	   M�     W�     k�  	   }�     ��     ��     ��     מ     �     �     )�     ?�     U�     j�      ��     ��     ��     ҟ     �     ��     �     #�  
   B�     M�  	   `�     j�     ~�     ��  �  ��  %   ��  1   ��     آ  L   ޢ  (   +�      T�  +   u�     ��  |   ��  ]   1�     ��     ��     Ĥ     Ѥ     ݤ  (   �  2   	�     <�     D�     I�     b�     w�     ��     ��     ��     ¥     ϥ    (Signup as freelancer &amp; get hired)  Comments  | All Rights Reserved %d seconds ago (Signup as company/service seeker &amp; post jobs) 0 Comments 01 02 03 04 1 Comment 5 A New Proposal Received! A Service has reported! A bit more details and its done A freelancer has reported! APP ASC About Us About a Day Ago %d Days Ago About a Minute Ago %d Minutes Ago About a Month Ago %d Months Ago About a Week Ago %d Weeks Ago About a year Ago %d years Ago About an Hour Ago %d Hours Ago Account Deleted Add Animation image Add Answer Add App store image. Add App store link. Leave it empty to hide. Add Brands Add Button Add Counter Add Description Add Dispute Add Employer Add FAQs Add Freelancer Add Google Play store image. Add Google Play store link. Leave it empty to hide. Add Link Add LinkedIn link. Leave it empty to hide. Add Micro Service Add New Badge Add New Category Add New Delivery Time Add New Department Add New Dispute Add New Employer Add New Freelancer Add New Language Add New Location Add New Micro Service Add New Package/Product Add New Project Add New Proposal Add New Report Add New Response Time Add New Review Add New Service order Add New Skill Add Number of latests posts. It's only working if  Posts ID's field is null. Add Package/Product Add Posts ID's with comma(,) separated e.g(15,21). Leave it empty to show latest posts. Add Project Add Proposal Add Report Add Review Add Service order Add Title Add URL Add Video image. Leave it empty to hide. Add banner images. Leave it empty to hide. Add button link Add button link. Leave it empty to hide. Add button title Add button title. Leave it empty to hide button. Add button title. Leave it empty to hide. Add description. Leave it empty to hide button. Add description. Leave it empty to hide description. Add description. leave it empty to hide. Add designation Add facebook link. Leave it empty to hide. Add google plus link. Leave it empty to hide. Add instagram link. Leave it empty to hide. Add name Add newsletter description. leave it empty to hide. Add newsletter title. leave it empty to hide. Add posts ID's Add posts ID's with comma seprated e.g (10,19) if News by selection is By item. Add question Add section description. Leave it empty to hide. Add section heading. Leave it empty to hide. Add section sub heading. Leave it empty to hide. Add section sub title. Leave it empty to hide. Add section title. Leave it empty to hide. Add services Add slide Add sub title Add sub title. leave it empty to hide. Add subtitle. leave it empty to hide. Add team Member Add team Partner Add text color. leave it empty to use default color. Add title Add title. leave it empty to hide. Add top freelancer 's with comma separated ID's e.g(12,20). Leave it empty to hide. Add twitter link. Leave it empty to hide. Add url. Leave it empty to hide. Add video Image Add video link. Leave it empty to hide. Add video title. Leave it empty to hide. Add video title. leave it empty to hide. Add video url. Leave it empty to hide. Add video url. leave it empty to hide. Add work process Add youtube link. Leave it empty to hide. Admin Share Agree our terms and conditions All Badges All Category All Delivery Time All Department All Language All Location All Response Time All Rights Reserved All Skill Already Have an Account? Amentotech Amount App store App store image App store link Authentication Author Autoplay Available balance Back Badges Balance History Banner Brand Brands Button Link Button Style Button Text Button Title Button link Button title By Categories By item Cancelled Projects/Services Categories Categories settings Categories? Category Category Slider Charge Per hour Chat History Chat Messages Found! Chat messages found! Choose Category Choose File Classic View Two Columns Click for user details Click the button to send the offer to this freelancer Comments are off Complete your profile and get hired. Completed Projects/Services Congratulations Congratulations! You are hired. Congratulations! Your Job Has Posted Congratulations! Your Service Has Posted Content Continue Copyright Counter Title Create account Current Tab Current balance DESC Delete Delivery Time Department Description Details Disputes Download Download APPS Earnings Edit Edit Badge Edit Category Edit Delivery Time Edit Department Edit Dispute Edit Employer Edit Freelancer Edit Language Edit Location Edit Micro Service Edit Package/Product Edit Project Edit Proposal Edit Report Edit Response Time Edit Review Edit Service order Edit Skill Email Email Verification Code Email address is required. Email field is required. Employee and department fields are required. Employer Employer  Employer Reported Employers End Number Enter Email Here Enter Verification Code Explore Explore Categories Facebook Facebook link False Featured Feedback First Name First Name is required Followers Forgot Password Forgot password? Form type Four Columns Freelancer Freelancer Share Freelancers Full Gender field is required Get Password Get a quick help Go to dashboard Google Google Connect Google Play store image Google Play store link Google Plus Google plus link Google+ Grid Grid settings Heading Hello Help & Support Home Slider Home Slider V3 How It works How It works V3 How it work How to translate theme ID Image Alignment Import Dummy Import Employers/Freelancers Import From File Import User Import Users Instagram Instagram link Interval It will be used at post / Taxonomy detail page in URL as slug. Please use words without spaces. Items I’m looking for Job Cancelled Job Completed Job Posted Jobs Jobs Posted Jobs by categories Jobs by locations Jobs by skills Jobs by skills and location Join For a Good Start Join Now Join Now With Keep me logged in Language Languages Last Name Last Name is required. Latest Articles Left Link Target Linked Profile LinkedIn LinkedIn link Linkedin Links | Workreap List List settings List/Grid settings are below. Listing type Location Location field is required Location? Locations Login Login Now Login and registration is disabled by administrator. Logo Logos Loop Member Avatar Member Since Message deleted. Message field is required. Method Micro Services Minimum System Requirements Mobile APPS Mobile Apps More articles Name New Badge Name New Category Name New Delivery Time Name New Department Name New Dispute New Employer New Freelancer New Language Name New Location Name New Micro Service New Package/Product New Project New Proposal New Proposal Message Received New Report New Response Time Name New Review New Service Message Received New Service order New Skill Name New Tab News News By No No Dispute found No Dispute found in trash No Earnings avaliable. No Employer found No Employer found in trash No Freelancer found No Freelancer found in trash No Kiddies Please No Message Selected No Micro Service found No Micro Service found in trash No Packages/Products found No Packages/Products found in trash No Payouts avaliable. No Project found No Project found in trash No Proposal found No Proposal found in trash No Report found No Report found in trash No Review found No Review found in trash No Service order found No Service order found in trash No Tweets Found No kiddies Please No kiddies please No kiddies please. No list selected yet! please contact administrator No message selected to display No more messages... No service found. No. of employees you have Node.js for real-time chat Number of Tweets: Number of latests posts Number of posts Number of posts to show: Offer Received Ongoing Projects/Services Open First? Order Order by author Order by date Order by number of comments Order by post id Order by post name Order by title PHP Zip extension Should PHP version should be 7.0 or above Packages/Products Pagination option Parent Badges Parent Badges: Parent Category Parent Category: Parent Delivery Time Parent Delivery Time: Parent Department Parent Department: Parent Dispute Parent Employers Parent Freelancers Parent Language Parent Language: Parent Location Parent Location: Parent Micro Service Parent Package/Product Parent Projects Parent Proposals Parent Reports Parent Response Time Parent Response Time: Parent Reviews Parent Service order Parent Skill Parent Skill: Partner Password Password does not match. Password field is required Password length should be minimum 6 Password should not be empty. Password* PayPal Payment Method Payout Payout history of the month Payouts Pending Pinterest Play store Please add a valid email address. Please wait while you are redirecting to facebook for authorizations. Please wait while you are redirecting to google+ for authorizations. Popular Articles Popular posts | Workreap Post Order Posts ID's Posts/Items settings Price Processing Date Project Project Reported Project Type Project/Service Projects Proposal Proposal Message Received Proposal Received Proposal Submit Proposals Quick Links Random order Rating Rating: Regards Register Form Registration is disabled by administrator. Report Report Type Reported Posts Reported by Reports Resend Verification Code Response Time Retype Password* Review Review By Review Info Review To Review by Review to Reviews Rewrite URL Right Save Changes Search Badge Search Banner Search By ID Search Category Search Delivery Time Search Department Search Dispute Search Employer Search Freelancer Search Language Search Location Search Micro Service Search Now Search Packages/Product Search Project Search Proposal Search Report Search Response Time Search Review Search Service order Search Skill Search Users Search options Select Location to display. Select a project Select categories to display posts. Select categories to display. Select category services to display. Select location to display. Select logo Select month Select news by category or item. Select pagination option. Select posts Order. Select skills to display. Select year Send By Send a Project Offer Send offer Service Service  Service Cancelled Service Completed Service Message Received Service Order Service Posted Service order Services Settings Settings updated Share on Facebook Share on Google Share on Pinterest Share on Twitter Show latest tweets. Show links. Sign In Sign out Signup Skill Skills Skype Slider Small Some error occur, please try again later. Some error occur,please try again later. Some error occurs, please try again later Something went wrong. Sorry! No tweets found Sorry! you are restricted to perform this action on demo site. Speed Start Now Start Number Start as : Status Step 3 Style 1 Style 2 Sub Heading Sub Title Sub Video link Sub title Subscribe Successfully Successfully Logged in Symbol Teams Terms & Conditions Terms And Conditions Text Alignment Text color Thank You Thank you for purchasing the package! Thank you for registering This email already registered This is a computer generated payout slip This is where you can add new Dispute  This is where you can add new Micro Service This is where you can add new Reviews  This is where you can add new Service order This is where you can add new company This is where you can add new freelancer This is where you can add new projects  This is where you can add new proposal This is where you can add new reports  This is where you can add new tours to your store. This plugin have the core functionality for Workreap WordPress Theme Three Columns Title Title: Top Freelancers Total Earning True Tumbler Tweets Tweets | Workreap Twitter Twitter link Type message here Update Badge Update Category Update Delivery Time Update Department Update Language Update Location Update Response Time Update Skill Upload Background Image Upload Image Upload Image. leave it empty to hide. Upload banner images Upload image Upload image. Upload image. leave it empty to hide. Upload slide Image Upload video link. leave it empty to hide. Upload video thumbnail. leave it empty to hide. Url Use a social account for faster login or easy registration to directly get in to your account User Imported Successfully User Name User already exists. Please try another one. User type field is required. Username already registered Username or email Username should not be empty. Username: Verification code has sent on your email Verify Password field is required. Verify now Video Tutorial Video Url Video link Video title Video url View Dispute View Employer View Freelancer View Micro Service View Package/Product View Posts By. View Project View Proposal View Report View Review View Service order View User Profile View(s) Welcome & Greetings Welcome Note Welcome To Workreap What's New? Why do I need a code? Working Working Process Workreap Workreap Core Workreap Elements Workreap Popular posts Workreap is a Freelance Marketplace WordPress theme with some exciting features and excellent code quality. It has been designed and developed after thorough research to cater the requirements of people interested in building freelance marketplace or other similar projects. The design is contemporary but at the same time it focuses on the usability, visual hierarchy and aesthetics to ensure easy navigation for the end users Would you like to add your first Job? Wrong email/username or password. Yes You can view your dashboard by clicking below link You have not any privilege to view this page. You have received a new dispute You should agree to terms and conditions. Your Department? Your account have been created. Please verify your account through verification code, an email have been sent your email address. Your account have been created. Please wait while your account is verified by the admin. Your tagline goes here Youtube link float-left float-right hr https://themeforest.net/user/amentotech https://themeforest.net/user/amentotech/portfolio in logo max_execution_time = 300 max_input_time = 300 memory_limit = 300 not post_max_size = 100M upload_max_filesize = 100M user username Project-Id-Version: Workreap Core
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-12-23 12:00+0000
PO-Revision-Date: 2019-12-23 12:01+0000
Last-Translator: admin <etwordpress01@gmail.com>
Language-Team: Turkish
Language: tr_TR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3.2 (Freelancer olarak kayıt olun ve işe alın)
 Yorumlar | Tüm hakları Saklıdır
 % d saniye önce (Şirket / servis arayan ve iş ilanı olarak kayıt olun)
 0 Yorum 01
 02 03 04 1 yorum 5 Yeni Bir Teklif Alındı!
 Bir servis bildirdi!
 Biraz daha detay ve bitti
 Bir freelancer bildirmiştir!
 APP ASC Hakkımızda Bir Gün Önce  Yaklaşık bir dakika önce % d Dakika önce Yaklaşık bir ay önce % d Ay önce Yaklaşık bir hafta önce % d Hafta Önce Yaklaşık bir yıl önce % d yıl önce Yaklaşık bir saat önce % d Saat önce Hesap Silindi
 Animasyon resmi ekle Cevap ekle Uygulama mağazası resmi ekleyin. Uygulama mağazası bağlantısı ekle. Saklanmak için boş bırakın. Marka ekle Düğme ekle Sayaç ekle Açıklama ekle Anlaşmazlık ekle
 İşveren ekle
 SSS ekle Freelancer ekle
 Google Play Store görüntüsünü ekleyin. Google Play Store bağlantısını ekleyin. Saklanmak için boş bırakın. Link ekle LinkedIn bağlantısını ekle. Saklanmak için boş bırakın. Mikro Hizmet Ekleyin
 Yeni Rozet Ekle
 Yeni Kategori Ekle
 Yeni Teslim Süresi Ekleyin
 Yeni Bölüm Ekle
 Yeni Anlaşmazlık Ekle
 Yeni İşveren Ekleyin
 Yeni Freelancer Ekle
 Yeni Dil Ekle
 Yeni Konum Ekle
 Yeni Mikro Hizmet Ekleyin
 Yeni Paket / Ürün Ekleyin
 Yeni Proje Ekle
 Yeni Teklif Ekleyin
 Yeni Rapor Ekle
 Yeni Tepki Süresi Ekleme
 Yeni Yorum Ekle
 Yeni Servis siparişi ekle
 Yeni Beceri Ekle
 En son gönderilerin sayısını ekleyin. Yalnızca Mesaj Kimliği alanı boşsa çalışıyor. Paket / Ürün ekle
 Mesaj kimliklerini virgül (,) ayrılmış olarak, örneğin (15,21) ekleyin. Son gönderileri göstermek için boş bırakın. Proje ekle
 Teklif ekle
 Rapor ekle
 Yorum ekle
 Servis siparişi ekle
 Başlık ekle URL ekle Video görüntüsü ekleyin. Saklanmak için boş bırakın. Afiş görüntüleri ekleyin. Saklanmak için boş bırakın. Düğme bağlantısı ekle Düğme bağlantısı ekle. Saklanmak için boş bırakın. Düğme başlığı ekle Düğme başlığı ekle. Düğmeyi gizlemek için boş bırakın. Düğme başlığı ekle. Saklanmak için boş bırakın. Açıklama ekle Düğmeyi gizlemek için boş bırakın. Açıklama ekle Açıklamayı gizlemek için boş bırakın. Açıklama ekle saklamak için boş bırakın. Atama ekle Facebook linki ekleyin. Saklanmak için boş bırakın. Google plus bağlantısı ekleyin. Saklanmak için boş bırakın. İnstagram bağlantısı ekleyin. Saklanmak için boş bırakın. İsim ekle Bülten açıklaması ekleyin. saklamak için boş bırakın. Bülten başlığı ekleyin. saklamak için boş bırakın. Yayın kimliği ekle Seçime göre haberler öğeye göre ise, virgülle ayrılmış mesaj kimliklerini (ör. 10,19) ekleyin. Soru ekle Bölüm açıklaması ekleyin. Saklanmak için boş bırakın. Bölüm başlığı ekle. Saklanmak için boş bırakın. Bölüm alt başlığı ekle. Saklanmak için boş bırakın. Bölüm alt başlığı ekleyin. Saklanmak için boş bırakın. Bölüm başlığı ekle. Saklanmak için boş bırakın. Hizmet ekle Slayt ekle Alt başlık ekle Alt başlık ekle. saklamak için boş bırakın. Altyazı ekle. saklamak için boş bırakın. Takım üyesi ekle Takım Ortağı ekle Metin rengi ekleyin. varsayılan rengi kullanmak için boş bırakın. Başlık ekle Başlık ekle. saklamak için boş bırakın. Virgülle ayrılmış kimlikleriyle (örneğin, 12,20) en iyi freelancer ekleyin. Saklanmak için boş bırakın. Twitter link ekleyin. Saklanmak için boş bırakın. URL ekle Saklanmak için boş bırakın. Video ekle Resim Video linki ekle Saklanmak için boş bırakın. Video başlığı ekle. Saklanmak için boş bırakın. Video başlığı ekle. saklamak için boş bırakın. Video URL'si ekleyin. Saklanmak için boş bırakın. Video URL'si ekleyin. saklamak için boş bırakın. İş süreci ekle Youtube bağlantısı ekleyin. Saklanmak için boş bırakın. Admin Share
 Hüküm ve koşullarımızı kabul ediyorum
 Tüm Rozetler
 Tüm kategoriler
 Tüm Teslimat Süresi
 Tüm Bölüm
 Tüm dil
 Tüm konumlar
 Tüm Tepki Süresi
 Tüm hakları Saklıdır
 Tüm Beceri
 Zaten hesabınız var mı? Amentotech Miktar
 Uygulama mağazası Uygulama mağazası resmi Uygulama mağazası bağlantısı Doğrulama
 Yazar
 Otomatik oynatma Kalan bakiye
 Geri
 Rozetler
 Bakiye Geçmişi
 afiş Marka Markalar Düğme Bağlantısı Düğme Stili Düğme Metni Düğme Başlığı Düğme bağlantısı Düğme başlığı Kategorilere göre Öğeye göre İptal Edilen Projeler/Hizmetler Kategoriler
 Kategoriler ayarları Kategoriler? Kategori
 Kategori Slider Saat başına ücret
 Sohbet geçmişi Sohbet Mesajları Bulundu!
 Sohbet mesajları bulundu!
 Kategori Seçin Dosya seçin
 Klasik Görünüm İki Sütun Kullanıcı detayları için tıklayın
 Teklifi bu freelancera göndermek için düğmeye tıklayın
 Yorumlar kapalı Profilini tamamla ve işe alın.
 Tamamlanan Projeler / Hizmetler Tebrikler
 Tebrikler! İşe alındınız.
 Tebrikler! Mesleğiniz Gönderildi
 Tebrikler! Hizmetiniz Gönderildi
 içerik Devam et
 Telif hakkı
 Sayaç Başlığı Üye Ol Geçerli Sekme Mevcut bakiye
 DESC Sil Teslimat süresi
 Bölüm Açıklama ayrıntılar İhtilaflar İndir
 APPS indir Kazanç
 Düzenle
 Rozeti Düzenle
 Kategoriyi Düzenle
 Teslim Süresini Düzenle
 Bölümü Düzenle
 Anlaşmazlık Düzenle
 İşvereni Düzenle
 Freelancer Düzenle Dili Düzenle
 Konumunu düzenle
 Mikro Hizmeti Düzenle
 Paketi / Ürünü Düzenle
 Projeyi Düzenle
 Teklifi Düzenle
 Raporu Düzenle
 Yanıt Zamanını Düzenle
 Yorumu Düzenle
 Servis siparişini düzenle
 Beceri Düzenle
 E-posta
 E-posta Doğrulama Kodu
 E-posta adresi gerekli.
 E-posta alanı zorunludur.
 Çalışan ve departman alanları zorunludur.
 İşveren
 İşveren
 İşveren Raporlandı
 İşverenler
 Bitiş Numarası Buraya E-posta Girin
 Dogrulama kodunu giriniz
 keşfetmek Kategorileri Keşfet Facebook
 Facebook bağlantısı Yanlış Öne Çıkan geri bildirim İsim
 İlk isim gerekli
 İzleyiciler
 Şifremi Unuttum Şifremi Unuttum Form türü Dört Sütun bağımsız yazar
 Freelancer Paylaş
 Freelancers Tam Cinsiyet alanı zorunludur
 Şifre al
 Yardım Al
 Kontrol paneline git
 Google Google Connect
 Google Play Store resmi Google Play Store bağlantısı Google Plus Google plus bağlantısı Google+
 Grid Izgara ayarları başlık Merhaba Yardım desteği
 Ev kaymak Ana Sayfa Slider V3 Nasıl çalışır Nasıl çalışır? V3 Nasıl çalışır Tema nasıl çevrilir
 İD Görüntü Hizalama İthalat kukla
 İşveren / Freelancers İthalat
 Dosyadan İçe Aktar
 Kullanıcıyı İçe Aktar
 Kullanıcıları İçe Aktar
 Instagram Instagram bağlantısı Aralık URL’deki post / Taxonomy detay sayfasında sümüklü böcek olarak kullanılacaktır. Lütfen boşluksuz kelimeler kullanın.
 Öğeler Arıyorum İş iptal edildi
 İş tamamlandı
 İş Gönderildi
 Meslekler Yayınlanan İşler
 Kategorilere göre işler Lokasyonlara göre işler Becerilere göre işler Becerilere ve yere göre işler İyi Bir Başlangıç ​​İçin Katılın
 Üye Ol
 Şimdi Katıl Beni Hatırla Dil
 Dil
 Soyadı
 Soyadı gerekli.
 En son makaleler Ayrıldı Bağlantı hedefi Bağlantılı Profil
 LinkedIn LinkedIn bağlantısı Linkedin
 Linkler | Workreap
 Liste Liste ayarları Liste / Izgara ayarları aşağıdadır. İlan türü Konum Konum alanı zorunludur
 Yer? Mekanlar
 GİRİŞ Şimdi giriş yap
 Giriş ve kayıt yönetici tarafından devre dışı bırakıldı.
 Logo logolar döngü Üye Avatarı Den beri üye
 Mesaj silindi.
 Mesaj alanı zorunludur.
 Yöntem Mikro Hizmetler
 minimum sistem gereksinimleri
 Mobil uygulamalar Mobil uygulamalar Daha fazla makale
 isim Yeni Rozet Adı
 Yeni Kategori Adı
 Yeni Teslimat Süresi Adı
 Yeni Bölüm Adı
 Yeni Anlaşmazlık
 Yeni işveren
 Yeni Freelancer
 Yeni Dil Adı
 Yeni Konum Adı
 Yeni Mikro Servis
 Yeni Paket / Ürün
 Yeni proje
 Yeni teklif
 Yeni Teklif Mesajı Alındı
 Yeni rapor
 Yeni Tepki Süresi Adı
 Yeni inceleme
 Yeni Servis Mesajı Alındı
 Yeni Servis siparişi
 Yeni Beceri Adı
 Yeni sekme Haber Haberler By Hayır
 Anlaşmazlık bulunamadı
 Çöpte Uyuşmazlık Bulunamadı
 DAİREMİZ Kazanç yok.
 İşveren bulunamadı
 Çöp kutusunda İşveren bulunamadı
 Freelancer bulunamadı
 Çöp kutusunda Freelancer bulunamadı
 Çocuk yok lütfen Mesaj Seçilmedi
 Mikro Servis bulunamadı
 Çöp kutusunda Mikro Hizmet bulunamadı
 Paket / Ürün bulunamadı
 Çöp Kutusunda Paket / Ürün bulunamadı
 DAİREMİZ Ödeme Yok.
 Proje bulunamadı
 Çöpte Proje bulunamadı
 Teklif bulunamadı
 Çöp kutusunda Teklif bulunamadı
 Rapor bulunamadı
 Çöp kutusunda rapor bulunamadı
 Yorum bulunamadı
 Çöpte yorum bulunamadı
 Servis siparişi bulunamadı
 Çöp kutusunda Hizmet siparişi bulunamadı
 Hiçbir Tweet Bulunamadı
 Çocuk yok lütfen Çocuk yok lütfen Çocuk yok lütfen. Henüz bir liste seçilmedi! lütfen yöneticiye başvurun
 Gösterilecek mesaj seçilmedi
 Başka mesaj yok ...
 Hizmet bulunamadı. Sahip olduğunuz çalışan sayısı
 Gerçek zamanlı sohbet için Node.js
 Tweet Sayısı:
 En son gönderilerin sayısı Mesaj sayısı Gösterilecek gönderi sayısı:
 Teklif Alındı
 Devam Eden Projeler/Hizmetler İlk açtın mı? Sipariş
 Yazara göre sırala Tarihe göre sırala Yorum sayısına göre sırala Gönderi kimliğine göre sırala Gönderi adına göre sırala Başlığa göre sırala PHP Zip uzantısı gerekir
 PHP sürümü 7.0 veya üstü olmalıdır
 Paketler / Ürünler
 Sayfalandırma seçeneği Ebeveyn Rozetleri
 Ana Rozetler:
 aile kategorisi
 Aile kategorisi:
 Ebeveyn Teslim Süresi
 Ebeveyn Teslim Süresi:
 Ebeveyn Departmanı
 Ana Daire:
 Ana Anlaşmazlık
 Ana İşveren
 Ebeveyn Freelancers Ana Dil
 Ana Dil:
 Üst Konum
 Üst Konum:
 Ana Mikro Hizmet
 Ana Paket / Ürün
 Ana Projeler
 Ana Teklifler
 Ana Raporlar
 Ebeveyn Tepki Süresi
 Ebeveyn Tepki Süresi:
 Üst Yorumlar
 Ebeveyn Hizmet siparişi
 Ebeveyn Beceri
 Üst Beceri:
 Ortak Şifre Şifre eşleşmiyor.
 Şifre alanı gerekiyor
 Şifre uzunluğu en az 6 olmalıdır
 Şifre boş olmamalıdır.
 Parola*
 PayPal
 Ödeme şekli
 Ödeme
 Ayın ödeme geçmişi Ödemeler
 kadar Pinterest Oyun mağazası Lütfen geçerli bir e-posta adresi ekleyin.
 Lütfen yetkilendirme için facebook'a yönlendirirken bekleyin.
 Lütfen yetkiler için google + 'ya yönlendirirken bekleyin.
 Popüler Makaleler
 Popüler yazılar | Vswep
 Siparişi Gönder Mesaj Kimliği Mesajlar/Öğeler ayarları Fiyat İşlem tarihi
 Proje Proje Raporlandı
 Proje tipi
 Proje / Hizmet
 Projeler
 Öneri
 Teklif Mesajı Alındı
 Teklif Alındı
 Teklif Gönder
 Teklif
 Hızlı Linkler
 Rastgele sıra Değerlendirme
 Değerlendirme:
 Saygılarımızla
 Kayıt Formu Kayıt işlemi yönetici tarafından devre dışı bırakıldı.
 Rapor
 Rapor türü
 Raporlanan Gönderiler
 Tarafından rapor edildi
 Raporlar
 Doğrulama Kodunu tekrar yolla
 Tepki Süresi
 Şifrenizi yeniden yazın*
 gözden geçirmek
 Tarafından incelemek
 İnceleme Bilgisi
 Gözden Geçir
 Tarafından incelemek
 İnceleme için
 Yorumlar
 URL'yi yeniden yaz
 Right Değişiklikleri Kaydet
 Rozet Ara
 Arama Başlığı Kimliğe Göre Arama
 Arama Kategorisi
 Arama Teslim Süresi
 Arama Departmanı
 Arama Anlaşmazlığı
 İşveren Ara
 Freelancer'da Ara
 Arama Dili
 Arama Yeri
 Hizmet Ara
 Şimdi araştır Arama Paketleri / Ürün
 Arama Projesi
 Arama Teklifi
 Arama Raporu
 Arama Tepki Süresi
 Arama İncelemesi
 Arama Hizmeti siparişi
 Arama Becerisi
 Kullanıcıları Ara
 Arama seçenekleri Görüntülenecek Konum'u seçin. Bir proje seç
 Gönderileri görüntülemek için kategorileri seçin. Görüntülenecek kategorileri seçin. Görüntülenecek kategori hizmetlerini seçin. Görüntülenecek yeri seçin. Logo seç Ay seç
 Haberleri kategoriye veya öğeye göre seçin. Sayfalama seçeneğini seçin. Mesajları seç Görüntülenecek becerileri seçin. Yıl seç
 Gönderen
 Proje Teklifi Gönder
 Teklif gönder
 Hizmet
 Hizmet Servis İptal Edildi
 Servis Tamamlandı
 Servis Mesajı Alındı
 Servis siparişi
 Servis Yayınlandı
 Servis siparişi Hizmetler Ayarlar
 Ayarlar güncellendi
 Facebook'ta Paylaş Google'da paylaş Pinterest'te paylaş Twitter'da paylaş En son tweet'leri göster.
 Bağlantıları göster.
 Giriş Yap Oturumu Kapat
 Kaydol
 Beceri
 Beceri
 Skype
 kaydırıcı Küçük Bazı hatalar meydana gelirse, lütfen daha sonra tekrar deneyin.
 Bazı hatalar meydana gelirse, lütfen daha sonra tekrar deneyin.
 Bazı hatalar oluşur, lütfen daha sonra tekrar deneyin
 Bir şeyler yanlış gitti.
 Afedersiniz! Tweet bulunamadı
 Afedersiniz! Bu eylemi demo sitesinde gerçekleştirmeniz sınırlandırılmıştır.
 hız Şimdi başla
 Başlangıç ​​numarası Olarak başla: Durum
 Aşama 3 Stil 1 Stil 2 Alt başlık Alt yazı Alt Video bağlantısı Alt yazı Başarıyla Abone Ol
 Başarıyla Giriş Yaptı
 sembol Takımlar Şartlar & Koşullar
 Şartlar ve koşullar Metin hizalama Metin rengi Teşekkür ederim
 Paketi satın aldığınız için teşekkür ederiz!
 kayıt olduğunuz için teşekkürler
 Bu e-posta zaten kayıtlı
 Bu bir bilgisayar tarafından oluşturulan ödeme makbuzu Burası yeni bir anlaşmazlık ekleyebileceğiniz yer
 Burası yeni Mikro Servis ekleyebileceğiniz yer
 Burası yeni Yorumlar ekleyebileceğiniz yerdir
 Burası yeni Servis siparişi ekleyebileceğiniz yer
 Burası yeni firma ekleyebileceğiniz yer
 Burası yeni freelancer ekleyebileceğiniz yer
 Burası yeni projeler ekleyebileceğiniz yer
 Burası yeni teklif ekleyebileceğiniz yer
 Burası yeni raporlar ekleyebileceğiniz yer
 Mağazana yeni turlar ekleyebilirsin.
 Bu eklenti Workreap WordPress Theme için temel işlevselliğe sahiptir
 Üç Sütun Başlık Başlık:
 En Çok İzlenenler Toplam Kazanç
 Doğru Tumbler Tweetler Tweet'ler | Workreap
 Twitter Twitter bağlantısı İletiyi buraya yazın
 Rozeti Güncelle
 Kategoriyi Güncelle
 Teslim Süresini Güncelle
 Güncelleme Departmanı
 Dili Güncelle
 Konumu Güncelle
 Yanıt Zamanını Güncelle
 Beceriyi Güncelle
 Arkaplan Resmini Yükle Fotoğraf yükleniyor Fotoğraf yükleniyor. saklamak için boş bırakın. Afiş resimlerini yükle Fotoğraf yükleniyor Fotoğraf yükleniyor. Fotoğraf yükleniyor. saklamak için boş bırakın. Slayt resmi yükle Upload video link. leave it empty to hide. Video küçük resmini yükleyin. saklamak için boş bırakın. Url Daha hızlı giriş yapmak veya doğrudan hesabınıza girmek için kolay kayıt için bir sosyal hesap kullanın Kullanıcı Başarıyla Alındı
 Kullanıcı adı
 Kullanıcı zaten var. Lütfen başka bir tane deneyin.
 Kullanıcı tipi alanı zorunludur.
 Kullanıcı zaten kayıtlı
 Kullanıcı adı ya da email Kullanıcı adı boş olmamalıdır.
 Kullanıcı adı:
 Doğrulama kodu e-postanıza gönderildi Doğrulama Şifresi alanı gerekiyor.
 Şimdi Doğrulayın
 Video öğretici
 Video Url Video bağlantısı Video başlığı Video url Anlaşmazlığı Görüntüle
 İşveren Görüntüle
 Freelancer Görüntüle
 Mikro Hizmeti Görüntüle
 Paketi / Ürünü Görüntüle
 Gönderileri Göster. Projeyi Görüntüle
 Teklifi Görüntüle
 Raporu görüntüle
 İncelemeyi Görüntüle
 Hizmet siparişini görüntüle
 Kullanıcı profilini gör
 Görünüm (ler) Hoşgeldin ve selamlar Hoş geldin notu Workreap'e Hoşgeldiniz
 Ne var ne yok?
 Neden bir koda ihtiyacım var? Çalışma Çalışma süreci Workreap
 Workreap Çekirdek
 Workreap Elemanları Popüler yazılar
 Workreap, bazı heyecan verici özelliklere ve mükemmel kod kalitesine sahip bir Serbest Piyasa WordPress temasıdır. Serbest pazar veya benzeri projeler inşa etmekle ilgilenen kişilerin gereksinimlerini karşılamak için kapsamlı bir araştırmadan sonra tasarlanmış ve geliştirilmiştir. Tasarım çağdaş olmakla birlikte, son kullanıcılar için kolay gezinmeyi sağlamak için kullanılabilirlik, görsel hiyerarşi ve estetik konularına odaklanmaktadır.
 İlk işinizi eklemek ister misiniz?
 Yanlış e-posta / kullanıcı adı veya şifre.
 Evet
 Kontrol panelinizi aşağıdaki linke tıklayarak görüntüleyebilirsiniz.
 Bu sayfayı görüntüleme yetkiniz yok. Yeni bir anlaşmazlık aldınız Şartlar ve koşulları kabul etmelisiniz.
 Senin bölümün?
 Hesabınız oluşturuldu. Lütfen hesabınızı doğrulama koduyla doğrulayın, e-posta adresinizi bir e-posta gönderildi. Hesabınız oluşturuldu. Lütfen hesabınız yönetici tarafından doğrulanırken bekleyin. Tagline'ınız buraya gidiyor
 Youtube bağlantısı -Yüzer sola Sağa çık sa https://themeforest.net/user/amentotech
 https://themeforest.net/user/amentotech/portfolio
 içinde logo max_execution_time = 300 max_input_time = 300 memory_limit = 300 değil post_max_size = 100M upload_max_filesize = 100M
 kullanıcı
 Kullanıcı adı
 